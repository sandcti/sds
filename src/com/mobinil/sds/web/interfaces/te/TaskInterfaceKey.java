package com.mobinil.sds.web.interfaces.te;

public class TaskInterfaceKey {
// ----------------Actions-----------------------

    public static final String ACTION_SHOW_TASKS = "show_tasks";
    public static final String ACTION_SHOW_CONTRACT_TASKS = "show_contract_tasks";
    public static final String ACTION_SHOW_DAILY_TASKS = "show_daily_tasks";
    public static final String ACTION_SHOW_ARBU_TASKS_STATUS = "show_arbu_tasks_status";
    public static final String ACTION_MANAGE_TASKS = "manage_tasks";
    public static final String ACTION_VIEW_ARBU_PAGE = "view_arbu_task";
    public static final String ACTION_DELETE_TASK = "delete_tasks";
    public static final String ACTION_SAVE_TASK = "action_edit_tasks";
    public static final String ACTION_SAVE_ARBU_TASK = "action_save_arbu_tasks";
    public static final String ACTION_NEW_EDIT_TASK = "new_edit_task";
    public static final String ACTION_UPLOAD_FILE = "action_upload_file";
    public static final String ACTION_PROCESS_FILE = "process_file";
    public static final String ACTION_PROCESS_BATCH = "process_batch";
    public static final String ACTION_COMPLETED_TASKS = "completed_task";
    public static final String ACTION_DELETE_COMPLETED_TASKS = "delete_comleted_tasks";
    public static final String ACTION_EXPORT_COMPLETED_TASKS = "export_completed_task";
    public static final String ACTION_ADD_NEW_TASK_CONTRACT = "add_new_task_contract";
    public static final String ACTION_ADD_NEW_DAILY_CONTRACT = "add_new_daily_contract";
    public static final String ACTION_CREATE_DAILY_CONTRACT = "create_daily_task";
    public static final String ACTION_CHECK_DB_BEFORE_SUBMIT = "check_sql_statement";
    public static final String ACTION_SAVE_IVR_TASK = "action_save_ivr_task";
    public static final String ACTION_SAVE_NOMAD_TASK = "action_save_nomad_task";
    public static final String ACTION_SAVE_NTRA_TASK = "action_save_ntra_task";
    public static final String ACTION_SHOW_IVR_TASKS_STATUS = "action_show_ivr_tasks_status";
    public static final String ACTION_SHOW_NOMAD_TASKS_STATUS = "action_show_nomad_tasks_status";
    public static final String ACTION_SHOW_NOMAD = "action_show_nomad";
    public static final String ACTION_SHOW_NTRA_TASKS_STATUS = "action_show_ntra_tasks_status";
//-------------------Vectors----------------------------
    public static final String VEC_TASKS = "task_vec";
    public static final String HASHMAP_BATCH_TYPES = "hashmap_batch_type";
    public static final String DTO_TASK = "task_dto";
    public static final String FILE_EXPORT = "file_export";
    public static final String Daily_DTO_FLY_VALUES = "daily_dto";
    public static final String POST_PAID_PARTITIONS_VEC = "post_partitions";
    public static final String PRE_PAID_PARTITIONS_VEC = "pre_partitions";
//--------------------Controls--------------------------------
    public static final String CONTROL_BUTTON_DELETE_TASKS = "del_tasks";
    public static final String CONTROL_BUTTON_ADD_TASKS = "add_task";
    public static final String CONTROL_BUTTON_EDIT_TASKS = "edit_tasks";
    public static final String CONTROL_BUTTON_UPLOAD_FILE = "upload_file";
    public static final String CONTROL_BUTTON_ADD_NEW_TASK = "add_new_task";
    public static final String CONTROL_BUTTON_EDIT_TASK = "edit_task";
    public static final String CONTROL_BUTTON_SUBMIT_FILE = "upload_file";
    public static final String CONTROL_TEXT_TASK_NAME = "task_name";
    public static final String CONTROL_TEXT_TASK_DESCRIPTION = "task_description";
    public static final String CONTROL_TEXT_TASK_FILE = "task_file";
    public static final String CONTROL_OPTION_INSERION_TYPE = "insertion_type";
    public static final String CONTROL_INPUT_NAME_TO_DATE = "toDate";
    public static final String CONTROL_INPUT_NAME_FROM_DATE = "fromDate";
    public static final String CONTROL_INPUT_DAY_STAER_FROM_DATE = "dayStart";
    public static final String CONTROL_OPTION_CIF = "option_cif";
    public static final String CONTROL_OPTION_SFR = "option_sfr";
    public static final String CONTROL_COMBOBOX_TYPE = "list_type";
    public static final String CONTROL_INPUT_FILE = "task_file";
    public static final String CONTROL_INPUT_NAME_CONTRACT_NAME = "contract_name";
    public static final String CONTROL_COMBOBOX_SEARCH = "search_type";
    public static final String CONTROL_COMBOBOX_UPDATE = "update_type";
    public static final String CONTROL_TASK_TYPE = "control_task_type";
    public static final String CONTROL_IVR_TASK_NAME = "control_ivr_task_name";
    public static final String CONTROL_NOMAD_TASK_NAME = "control_nomad_task_name";
    public static final String CONTROL_NTRA_TASK_NAME = "control_ntra_task_name";
    public static final String CONTROL_IVR_TASK_TIME_DATA = "control_ivr_task_time_data";
    public static final String CONTROL_NTRA_TASK_TIME_DATA = "control_ntra_task_time_data";
    public static final String CONTROL_NOMAD_TASK_TIME_DATA = "control_nomad_task_time_data";
    public static final String ACTION_VIEW_NEW_TASK_PAGE = "view_new_task";
    public static final String ACTION_SAVE_NEW_TASK = "save_new_task";
    public static final String CONTROL_NEW_TASK_NAME = "control_new_task_name";
    public static final String CONTROL_NEW_TASK_TYPE = "control_new_task_type";
//--------------------------paramters------------------------------------------
    public static final String CONTROL_HIDDEN_TASK_ID = "task_id_param";
    public static final String PARAM_UPDATE_CUSTOMIZED_SEARCH_SELECT_LIST = "search_select_list";
    public static final String PARAM_UPDATE_CUSTOMIZED_UPDATE_SELECT_LIST = "update_select_list";
    public static final String ACTION_SHOW_AUTH_AGENT_TASK = "view_auth_agent_task";
//
//public static final String CONTROL_HIDDEN_TASK_ID = "task_id_param";
//
//public static final String CONTROL_HIDDEN_TASK_ID = "task_id_param";
}