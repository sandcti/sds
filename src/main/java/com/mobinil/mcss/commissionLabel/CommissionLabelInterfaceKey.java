/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.mcss.commissionLabel;

/**
 *
 * @author Gado
 */
public class CommissionLabelInterfaceKey {

    public static final String CONTROL_TEXT_COMMISSION_NAME = "commission_name";
    public static final String CONTROL_TEXT_COMMISSION_ID = "commission_id";
    public static final String CONTROL_TEXT_COMMISSION_STATUS = "commission_status";
    public static final String CONTROL_TEXT_COMMISSION_CHANNEL = "commission_channel";
    public static final String CONTROL_TEXT_COMMISSION_TYPE = "commission_type";
    public static final String CONTROL_TEXT_COMMISSION_CATEGORY = "commission_category";
    public static final String CONTROL_TEXT_COMMISSION_DP = "commission_dp";
    public static final String CONTROL_TEXT_COMMISSION_CATEGORY_NAME = "commission_category_name";
    public static final String CONTROL_TEXT_COMMISSION_CATEGORY_DESC = "commission_category_desc";
    public static final String CONTROL_TEXT_COMMISSION_SUBTRACT_ID = "subtractcomId";
    public static final String CONTROL_TEXT_COMMISSION_START_DATE = "commission_start_date";
    public static final String CONTROL_TEXT_COMMISSION_END_DATE = "commission_end_date";
    public static final String CONTROL_TEXT_COMMISSION_DATA_VIEW = "commission_data_view";
    public static final String CONTROL_TEXT_COMMISSION_DESCRIPTION = "commission_description";
    public static final String CONTROL_HIDDEN_CHANNEL_ID = "channel_id";
    public static final String CONTROL_TEXT_CHANNEL_NAME = "channel_NAME";
    public static final String CHANNEL_VECTOR = "channel_vec";
    public static final String USER_CHANNEL_VECTOR = "user_channel_vec";
    public static final String CHANNEL_MODEL = "channel_model";
    public static final String COMMISSION_MODEL = "commission_model";
    public static final String ACTION_COMMISSION_INCOMING_ACTION = "commission_incoming_action";
    public static final String ACTION_COMMISSION_SAVE_CHANNELS = "commission_save_channels";
    public static final String ACTION_COMMISSION_VIEW_CHANNELS = "commission_view_channels";
    public static final String ACTION_COMMISSION_EDIT_CHANNEL = "commission_edit_channels";
    public static final String ACTION_COMMISSION_NEW_CHANNEL = "commission_new_channels";
    public static final String ACTION_COMMISSION_USER_CHANNELS = "commission_user_channels";
    public static final String ACTION_COMMISSION_ASSIGN_CHANNEL_TO_USER = "commission_assign_channels_to_user";
    public static final String ACTION_COMMISSION_SAVE_NEW_COMMISSION = "commission_save_new_commission";
    public static final String ACTION_COMMISSION_SAVE_NEW_COMMISSION_NO_PARAM = "commission_save_new_commission_no_param";
    public static final String ACTION_COMMISSION_SAVE_NEW_COMMISSION_PARAM = "commission_save_new_commission_param";
    public static final String ACTION_VIEW_DCM_PAYMENTS = "view_dcm_payments";
    public static final String ACTION_VIEW_COMMISSION_LABELS = "view_commission_labels";
    public static final String ACTION_DELETE_ALL_LABEL_DETAILS = "delete_all_label_details";
    public static final String ACTION_SHOW_LABEL_DATA = "show_label_data";
    public static final String ACTION_ADD_NEW_LABEL = "add_new_label";
    public static final String ACTION_UPDATE_LABEL_DATA = "update_label_data";
    public static final String ACTION_SAVE_LABEL_DATA = "save_label_data";
    public static final String ACTION_VIEW_LABEL_DETAILS_DATA = "view_label_details_data";
    public static final String ACTION_EXPORT_LABEL_DETAILS_DATA = "export_label_details_data";
    public static final String ACTION_DELETE_UPLOAD_FILE = "delete_upload_file";
    public static final String ACTION_DELETE_UPLOAD_FILE_PROCESS = "delete_upload_file_process";
    public static final String ACTION_SAVE_LABEL_DETAILS_DATA = "save_label_details_data";
    public static final String ACTION_UPLOAD_COMMISSION_DATA = "upload_commission_data";
    public static final String ACTION_UPLOAD_COMMISSION_DATA_PROCESS = "upload_commission_data_process";
    public static final String ACTION_SEARCH_DCM_PAYMENTS = "search_dcm_payments";
    public static final String CONTROL_TEXT_COMMISSION_START_DATE_FROM = "commission_start_date_from";
    public static final String CONTROL_TEXT_COMMISSION_END_DATE_FROM = "commission_end_date_from";
    public static final String CONTROL_TEXT_COMMISSION_START_DATE_TO = "commission_start_date_to";
    public static final String CONTROL_TEXT_COMMISSION_END_DATE_TO = "commission_end_date_to";
    public static final String CONTROL_TEXT_COMMISSION_FACTOR_NAME = "commission_factor_name";
    public static final String CONTROL_TEXT_COMMISSION_FACTOR_VALUE = "commission_factor_value";
    public static final String CONTROL_TEXT_COMMISSION_BASE = "commission_base";
    public static final String CONTROL_TEXT_COMMISSION_DATA_VIEW_TYPE = "commission_data_view_type";
    public static final String DATA_VIEW_TYPE_A = "cross_tab";
    public static final String DATA_VIEW_TYPE_B = "normal";
    public static final String COMMISSION_DATA_VIEW_PARAMETER = "commission_data_view_parameter";
    public static final String ACTION_COMMISSION_SEARCH_COMMISSION = "commission_search_commission";
    public static final String ACTION_VIEW_READY_COMMISSION = "view_ready_commission";
    public static final String ACTION_VIEW_CLOSED_COMMISSION = "view_closed_commission";
    public static final String ACTION_VIEW_PREPARING_COMMISSION = "view_preparing_commission";
    public static final String ACTION_COMMISSION_EDIT_COMMISSION = "commission_edit_commission";
    public static final String ACTION_COMMISSION_ADD_NEW_COMMISSION = "commission_add_new_commission";
    public static final String ACTION_COMMISSION_SAVE_COMMISSION_CATEGORY = "commission_save_commission_category";
    public static final String ACTION_COMMISSION_SEARCH_PAYMENT = "commission_search_payment";
    public static final String ACTION_COMMISSION_EDIT_PAYMENT = "commission_edit_payment";
    public static final String ACTION_COMMISSION_ADD_NEW_PAYMENT = "commission_add_new_payment";
    public static final String ACTION_COMMISSION_VIEW_PAYMENT = "commission_view_commission";
    public static final String ACTION_COMMISSION_EXPORT_PAYMENT = "commission_export_payment";
    public static final String ACTION_COMMISSION_MANAGE_CATEGORY = "commission_manage_category";
    public static final String ACTION_VIEW_DRIVING_PLAN_COMMISSION_DETAIL = "view_dp_commission_detail";
    public static final String ACTION_COMMISSION_CREATE_NEW_COMMISSION = "create_new_commission";
    public static final String ACTION_COMMISSION_CREATE_NEW_PAYMENT = "create_new_payment";
    public static final String ACTION_SEARCH_PAYMENT = "search_payment";
    public static final String ACTION_SEARCH_COMMISSION = "search_commission";
    public static final String ACTION_COMMISSION_CREATE_NEW_COMMISSION_CATEGORY = "create_new_commission_category";
    public static final String ACTION_COMMISSION_EDIT_COMMISSION_CATEGORY = "edit_commission_category";
    public static final String ACTION_DELETE_COMMISSION = "delete_commission";
    public static final String ACTION_UPDATE_COMMISSION_STATUS = "update_commission_status";
    public static final String ACTION_COMMISSION_FACTORS = "commission_factors";
    public static final String ACTION_SAVE_COMMISSION_FACTORS = "save_commission_factors";
    public static final String ACTION_EXPORT_COMMISSION_TO_EXCEL = "export_commission_to_excel";
    public static final String ACTION_VIEW_COMMISSION = "view_commission";
    public static final String VECTOR_COMMISSION_TYPE = "vector_commission_type";
    public static final String VECTOR_COMMISSION_TYPE_CATEGORY = "vector_commission_type_category";
    public static final String MAP_COMMISSION_CATEGORY_DRIVING_PLAN = "map_commission_category_driving_plan";
    public static final String VECTOR_COMMISSION_STATUS = "vector_commission_status";
    public static final String VECTOR_PAYMENT_STATUS = "vector_payment_status";
    public static final String VECTOR_COMMISSION_SEARCH_RESULT = "vector_commission_search_result";
    public static final String MODEL_EDIT_CATEGORY_MODEL = "edit_category_model";
    public static final String VECTOR_COMMISSION_FACTORS = "vector_commission_factors";
    public static final String VECTOR_COMMISSION_FACTORS_NOT_IN_DP = "vector_commission_factors_not_in_dp";
    public static final String INPUT_TEXT_DCM_CODE = "text_dcm_code";
    public static final String INPUT_HIDDEN_DCM_CODE = "hidden_dcm_code";
    public static final String INPUT_HIDDEN_LABEL_ID = "hidden_label_id";
    public static final String INPUT_HIDDEN_TABLE_ID = "hidden_table_id";
    public static final String INPUT_TEXT_LABEL_NAME = "label_name";
    public static final String INPUT_TEXT_LABEL_DESCRIPTION = "label_description";
    //////////////////////////////////////////////////
    public static final String INPUT_RADIO_RADIOCATEGORY1 = "radio_category";
//////////////////////////////////////////////////////////////////
    public static final String INPUT_HIDDEN_CATEGORY_ID = "hidden_category_id";
    public static final String INPUT_HIDDEN_TYPE_ID = "hidden_type_id";
    public static final String INPUT_HIDDEN_COMMISSION_ACTION = "hidden_commission_action";
    public static final String INPUT_HIDDEN_DELETE_COMMISSION = "Delete";
    public static final String INPUT_HIDDEN_PAY_COMMISSION = "Pay";
    public static final String INPUT_HIDDEN_OPEN_COMMISSION = "Open";
    public static final String INPUT_HIDDEN_CLOSE_COMMISSION = "Close";
    public static final String INPUT_HIDDEN_EDIT_COMMISSION = "Edit";
    public static final String INPUT_HIDDEN_ALL_COMMISSION = "All";
    public static final String INPUT_HIDDEN_COMMISSION_STATUS = "hidden_commission_status";
    public static final String INPUT_HIDDEN_COMMISSION_ID = "hidden_commission_id";
    public static final String INPUT_HIDDEN_COMMISSION_TYPE_ID = "hidden_commission_type_id";
    public static final String ACTION_UPLOAD_FILES = "upload_files";
    public static final String SAVED_COMMISSION_ID = "SAVED_COMMISSION_ID";
    public static final String INPUT_HIDDEN_USER_VECTOR = "hidden_user_vector";
    public static final String RATED_ACTIVITY_DATE_FROM = "rated_activity_date_from";
    public static final String RATED_ACTIVITY_DATE_TO = "rated_activity_date_to";
    public static final String RATED_ACTIVITY_MONTH_FROM = "rated_activity_month_from";
    public static final String RATED_ACTIVITY_MONTH_TO = "rated_activity_month_to";
    public static final String RATED_ACTIVITY_YEAR_FROM = "rated_activity_year_from";
    public static final String RATED_ACTIVITY_YEAR_TO = "rated_activity_year_to";
    public static final String RATED_ACTITITY_TABLE = "56";
    public static final String ACTION_UPLOAD_RATED_ZIP_FILE = "action_upload_rated_zip_file";
    public static final String NUMBER_OF_PASS_RECORDS = "number_of_pass_records";
    public static final String NUMBER_OF_FAILD_RECORDS = "number_of_faild_records";
    public static final String VECTOR_OF_FAILD_SIM = "vector_of_faild_sim";
    public static final String VECTOR_RATED_FILES = "vector_rated_files";
    public static final String RATED_FILE_ID = "rated_file_id";
    public static final String ACTION_DELETE_RATED_ACTIVITY = "action_delete_rated_activity";
    public static final String DELETE_UPLOAD_RATED_ACTIVITY = "delete_upload_rated_activity";
    public static final String ACTION_VIEW_RATED_ACTIVITY = "action_view_rated_activity";
    public static final String LABEL_MODEL = "label_model";
}
