/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.mcss.level_relation_management;

/**
 *
 * @author SAND
 */
public class LevelRelationManagementInterfaceKey {

    public static final String ACTION_UPLOAD_WAREHOUSE_TO_LEVEL_ZERO = "upload_warehouse_to_level_zero";
    public static final String ACTION_UPLOAD_WAREHOUSE_TO_LEVEL_ZERO_PROCESS = "upload_warehouse_to_level_zero_process";
    public static final String CONTROL_SELECT_WAREHOUSE = "control_select_warehouse";
    public static final String CONTROL_WAREHOUSE_DATA = "control_warehouse_data";
    public static final String ACTION_LEVEL_ZERO_TO_LEVEL_ONE = "upload_level_zero_to_level_one";
    public static final String ACTION_LEVEL_ZERO_TO_LEVEL_ONE_PROCESS = "upload_level_zero_to_level_one_process";
    public static final String CONTROL_SELECT_LEVEL_ZERO = "control_select_level_zero";
    public static final String CONTROL_LEVEL_ZERO_DATA = "control_level_zero_data";
    public static final String ACTION_VIEW_LEVEL_ZERO_TO_LEVEL_ONE = "view_level_zero_to_level_one";
    public static final String VECTOR_LEVEL_ZERO_TO_LEVEL_ONE_DATA = "vector_level_zero_to_level_one_data";
    public static final String CONTROL_SHOW_LEVEL_ZERO_TO_LEVEL_ONE_CHECKBOX = "show_level_zero_to_level_one_checkbox_";
    public static final String ACTION_EDIT_LEVEL_ZERO_TO_LEVEL_ONE = "edit_level_zero_to_level_one";
    public static final String ACTION_DELETE_LEVEL_ZERO_TO_LEVEL_ONE = "delete_level_zero_to_level_one";
    public static final String INPUT_HIDDEN_LEVEL_ZERO_TO_LEVEL_ONE_ID = "hidden_level_zero_to_level_one_id";
    //NR - SDS
    public static final String ACTION_CHANGE_UPLOADED_EMAIL_DATES = "change_uploaded_email_dates";
    public static final String ACTION_DELETE_UPLOADED_EMAIL_DATES="delete_uploaded_email_records";
    public static final String CONTROL_TEXT_SENDER_EMAIL        = "sender_email";
    public static final String CONTROL_TEXT_EMAIL_POS_CODE    = "email_pos_code";
    public static final String CONTROL_TEXT_EMAIL_FROM_DATE  = "email_from_date";
    public static final String CONTROL_TEXT_EMAIL_TO_DATE  = "email_to_date";
    public static final String ACTION_EMAIL_SEARCH = "email_search";
    public static final String VECTOR_EMAIL_SEARCH_RESULT="email_search_result";
    public static final String INPUT_HIDDEN_EMAIL_ID = "hidden_email_id";
    //End - NR - SDS
}
