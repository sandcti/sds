/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.mcss.dp.dpManagement.controller;

/**
 *
 * @author Gado
 */
public class DPInterfaceKey {
    public static String channelList = "Channels_list";
    public static String dcmLevel = "dcm_level_list";
    public static String dcmPaymentLevel = "dcm_payment_level_list";
    public static String dpStatusList = "dp_status_list";
    public static String dpPaymentTypeList = "dp_payment_type_list";
    public static String dpCommissionCategoryList = "dp_commission_category_list";
    public static String monthsList = "months";
    public static String yearsList = "years";
}
