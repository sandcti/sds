/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.mcss.commission;

/**
 *
 * @author Shady Akl
 */
public class CommissionIIIInterfaceKey {

    public static final String VECTOR_COMMISSION_TYPE = "vector_commission_type";
    public static final String VECTOR_COMMISSION_TYPE_CATEGORY = "vector_commission_type_category";
    public static final String VECTOR_COMMISSION_STATUS = "vector_commission_status";
    public static final String ACTION_COMMISSION_SAVE_NEW_COMMISSION_PARAM = "commission_save_new_commission_param";
    public static final String INPUT_HIDDEN_COMMISSION_ACTION = "hidden_commission_action";
    public static final String INPUT_HIDDEN_ALL_COMMISSION = "All";
    public static final String INPUT_HIDDEN_COMMISSION_ID = "hidden_commission_id";
    public static final String SAVED_COMMISSION_ID = "SAVED_COMMISSION_ID";
    public static final String INPUT_HIDDEN_COMMISSION_STATUS = "hidden_commission_status";
    public static final String ACTION_REVIEW_COMMISSION = "action_review_commission";
    public static final String VECTOR_COMMISSION_REVIEW = "vector_commission_review";
}
