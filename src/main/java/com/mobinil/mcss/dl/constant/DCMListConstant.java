/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.mcss.dl.constant;


/**
 *
 * @author mabdelaal
 */
public interface DCMListConstant {
    
    
    
    
    public static final String DCM_LIST_REQUEST_MAPPING="/dl";

    public static final Long DCM_LIST_STATUS_ACTIVE = 1L;
    public static final Long DCM_LIST_STATUS_PENDING = 2L;
    public static final Long DCM_LIST_STATUS_DEACTIVE = 3L;
    public static final Long DCM_LIST_STATUS_DELETE = 4L;
    public static final Long DCM_LIST_STATUS_NEW = 5L;
   
    public static final String UNDER_SCORE = "_";
    public static final String URL_SEPARATOR = "/";

}
