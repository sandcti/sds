package com.mobinil.sds.web.interfaces.hlp;

public interface HLPInterfaceKey 
{
  public static final String ACTION_HLP_CREATE_NEW_CASE = "hlp_create_new_case";
  public static final String ACTION_HLP_ADMIN_VIEW_CASES = "hlp_admin_view_cases";
  public static final String ACTION_HLP_ADMIN_SEARCH_CASE = "hlp_admin_search_case";
  public static final String ACTION_HLP_ADMIN_FORWARD_CASE = "hlp_admin_forward_case";
  public static final String ACTION_HLP_ADMIN_VIEW_CASE_DETAIL = "hlp_admin_view_case_detail";
  public static final String ACTION_HLP_VIEW_CASES = "hlp_view_cases";
  public static final String ACTION_HLP_VIEW_CASES_USER_IN_CC = "hlp_view_cases_user_in_cc";
  public static final String ACTION_HLP_VIEW_CASE_DETAIL = "hlp_view_case_detail";
  public static final String ACTION_HLP_VIEW_CASE_DETAIL_CC_USER = "hlp_view_case_detail_cc_user";
  public static final String ACTION_HLP_CREATE_NEW_MISSION = "hlp_create_new_mission";
  public static final String ACTION_HLP_ADMIN_VIEW_MISSIONS = "hlp_admin_view_missions";
  public static final String ACTION_HLP_VIEW_MISSIONS = "hlp_view_missions";
  public static final String ACTION_HLP_ASSIGN_MISSION_USERS = "hlp_assign_mission_users";
  public static final String ACTION_HLP_EDIT_MISSION = "hlp_edit_mission";
  public static final String ACTION_HLP_UPLOAD_MISSION_TARGET_USERS = "hlp_upload_mission_target_users";
  public static final String ACTION_HLP_UPLOAD_MISSION_TARGET_USERS_PROCESS = "hlp_upload_mission_target_users_process";
  public static final String ACTION_HLP_VIEW_MISSION_DETAILS = "hlp_view_mission_details";
  public static final String ACTION_HLP_CREATE_NEW_CASE_ENTER_POS = "hlp_create_new_case_enter_pos";
  public static final String ACTION_HLP_VIEW_CASE_DETAIL_USER_IN_CC = "hlp_view_case_detail_user_in_cc";
  
  public static final String ACTION_HLP_SAVE_NEW_CASE = "hlp_save_new_case";
  public static final String ACTION_HLP_UPDATE_CASE_STATUS = "hlp_update_case_status";
  public static final String ACTION_HLP_SEARCH_CASE = "hlp_search_case";
  public static final String ACTION_HLP_FORWARD_CASE = "hlp_forward_case";
  public static final String ACTION_HLP_VIEW_CASE_HISTORY = "hlp_view_case_history";
  public static final String ACTION_HLP_UPDATE_MISSION_DETAILS = "hlp_update_mission_details";
  public static final String ACTION_HLP_SAVE_MISSION_USERS = "hlp_save_mission_users";
  public static final String ACTION_HLP_SAVE_NEW_MISSION = "hlp_save_new_mission";
  public static final String ACTION_HLP_UPDATE_MISSION_STATUS = "hlp_update_mission_status";
  public static final String ACTION_HLP_ADMIN_SEARCH_MISSION = "hlp_admin_search_mission";
  public static final String ACTION_HLP_SEARCH_MISSION = "hlp_search_mission";
  public static final String ACTION_HLP_SELECT_MISSION_TARGET_USER = "hlp_select_mission_target_user";
  public static final String ACTION_HLP_VIEW_TARGET_USER_SURVEY = "hlp_view_target_user_survey";
  public static final String ACTION_HLP_VIEW_MISSION_DETAILS_USER_LIST = "hlp_view_mission_details_user_list";
  public static final String ACTION_HLP_VIEW_MISSION_DETAILS_DCM_LIST = "hlp_view_mission_details_dcm_list";
  public static final String ACTION_HLP_SEARCH_CASE_USER_IN_CC = "hlp_search_case_user_in_cc";
  public static final String ACTION_HLP_UPDATE_CASE_DETAIL = "hlp_update_case_detail";
  public static final String ACTION_HLP_SAVE_UPDATED_CASE = "hlp_save_updated_case";
  public static final String ACTION_HLP_ADD_CASE_INFO_ELEMENT = "hlp_add_case_info_element";
  public static final String ACTION_HLP_SAVE_CASE_INFO_ELEMENT = "hlp_save_case_info_element";
  public static final String ACTION_HLP_EDIT_CASE_INFO_ELEMENT = "hlp_edit_case_info_element";
  public static final String ACTION_HLP_UPDATE_CASE_INFO_ELEMENT = "hlp_update_case_info_element";
  
  public static final String ACTION_HLP_CASE_TYPE_CATEGORY_MANAGEMENT = "hlp_case_type_category_management";
  public static final String ACTION_HLP_CASE_CATEGORY_MANAGEMENT = "hlp_case_category_management";
  public static final String ACTION_HLP_CASE_TYPE_MANAGEMENT = "hlp_case_type_management";
  public static final String ACTION_HLP_CASE_WARNING_MANAGEMENT = "hlp_case_warning_management";
  public static final String ACTION_HLP_EDIT_CASE_TYPE_CATEGORY = "hlp_edit_case_type_category";
  public static final String ACTION_HLP_ADD_CASE_TYPE_CATEGORY = "hlp_add_case_type_category";
  public static final String ACTION_HLP_EDIT_CASE_CATEGORY = "hlp_edit_case_category";
  public static final String ACTION_HLP_ADD_CASE_CATEGORY = "hlp_add_case_category";
  public static final String ACTION_HLP_EDIT_CASE_TYPE = "hlp_edit_case_type";
  public static final String ACTION_HLP_ADD_CASE_TYPE = "hlp_add_case_type";
  public static final String ACTION_HLP_EDIT_CASE_WARNING = "hlp_edit_case_warning";
  public static final String ACTION_HLP_ADD_CASE_WARNING = "hlp_add_case_warning";
  public static final String ACTION_HLP_SAVE_CASE_TYPE_CATEGORY = "hlp_save_case_type_category";
  public static final String ACTION_HLP_SAVE_CASE_CATEGORY = "hlp_save_case_category";
  public static final String ACTION_HLP_SAVE_CASE_TYPE = "hlp_save_case_type";
  public static final String ACTION_HLP_SAVE_CASE_WARNING = "hlp_save_case_warning";
  public static final String ACTION_HLP_DELETE_CASE_TYPE_CATEGORY = "hlp_delete_case_type_category";
  public static final String ACTION_HLP_DELETE_CASE_CATEGORY = "hlp_delete_case_category";
  public static final String ACTION_HLP_DELETE_CASE_TYPE  = "hlp_delete_case_type";
  public static final String ACTION_HLP_DELETE_CASE_WARNING = "hlp_delete_case_warning";

  public static final String ACTION_HLP_DELETE_DCM_FROM_MISSION = "hlp_delete_dcm_from_mission";
  public static final String ACTION_HLP_VIEW_USER_MISSION_DCM_LIST = "hlp_view_user_mission_dcm_list";
  public static final String ACTION_HLP_GET_RANDOM_INCOMPLETED_DCM = "hlp_get_random_incompleted_dcm";
  public static final String ACTION_SHOW_POS_CASES = "show_pos_cases";
  public static final String ACTION_VIEW_POS_CASES = "view_pos_cases";
  
  public static final String PAGE_HEADER = "page_header";

  ////////////////////////////////////////////////////////////////////////
  /*                 HLP Controls                 /
  *//////////////////////////////////////////////////////////////////////
  public static final String INPUT_SELECT_RECEIVER_ID = "select_receiver_id";
  public static final String INPUT_SELECT_CC_RECEIVER_EMAIL = "select_cc_receiver_email";
  public static final String INPUT_TEXT_CC_RECEIVER_EMAIL = "text_cc_receiver_email";
  public static final String INPUT_HIDDEN_CC_RECEIVER_ID = "hidden_cc_receiver_id";
  public static final String INPUT_SELECT_CATEGORY_ID = "select_category_id";
  public static final String INPUT_SELECT_TYPE_ID = "select_type_id";
  public static final String INPUT_SELECT_SUPER_TYPE_ID = "select_super_type_id";
  public static final String INPUT_SELECT_PRIORITY_ID = "select_priority_id";
  public static final String INPUT_TEXT_CASE_TITLE = "text_case_title";
  public static final String INPUT_TEXTAREA_CASE_DESCRIPTION = "textarea_case_description";
  public static final String INPUT_TEXTAREA_CASE_RECEIVER_COMMENT = "textarea_case_receiver_comment";
  public static final String INPUT_SELECT_STATUS_ID = "select_status_id";
  public static final String INPUT_SELECT_STATUS_WARNING_ID = "select_status_warning_id";
  public static final String INPUT_TEXT_STATUS_WARNING_NAME = "text_status_warning_name";
  public static final String INPUT_HIDDEN_CASE_ID = "hidden_case_id";
  public static final String INPUT_HIDDEN_DCM_CODE = "hidden_dcm_code";

  public static final String INPUT_HIDDEN_CASE_TITLE = "hidden_case_title";  
  public static final String INPUT_HIDDEN_RECEIVER_ID = "hidden_receiver_id";
  public static final String INPUT_HIDDEN_CASE_DESCRIPTION = "hidden_case_description";
  public static final String INPUT_HIDDEN_CASE_PRIORITY_ID = "hidden_case_priority_id";
  public static final String INPUT_HIDDEN_CASE_CATEGORY_ID = "hidden_case_category_id";
  public static final String INPUT_TEXT_CASE_CATEGORY_NAME = "text_case_category_name";
  public static final String INPUT_HIDDEN_CASE_TYPE_ID = "hidden_case_type_id";
  public static final String INPUT_TEXT_CASE_TYPE_NAME = "text_case_type_name";
  public static final String INPUT_HIDDEN_CASE_STATUS_ID = "hidden_case_status_id";
  public static final String INPUT_HIDDEN_CASE_STATUS_WARNING_ID = "hidden_case_status_warning_id";

  public static final String INPUT_TEXT_DIAL_NUMBER = "text_dial_number";
  public static final String INPUT_SELECT_DIAL_NUMBER_TYPE_ID = "select_dial_number_type_id";
  public static final String INPUT_SELECT_CASE_DIRECTION_ID = "select_case_direction_id";
  
  public static final String INPUT_SEARCH_TEXT_CASE_TITLE = "search_text_case_title";
  public static final String INPUT_SEARCH_SELECT_CASE_TYPE_ID = "search_select_case_type_id";
  public static final String INPUT_SEARCH_SELECT_CASE_CATEGORY_ID = "search_select_case_category_id";
  public static final String INPUT_SEARCH_SELECT_CASE_STATUS_ID = "search_select_case_status_id";
  public static final String INPUT_SEARCH_TEXT_CASE_SENDER_NAME = "search_text_case_sender_name";
  public static final String INPUT_SEARCH_TEXT_CASE_SENDED_FROM = "search_text_case_sended_from";
  public static final String INPUT_SEARCH_TEXT_CASE_SENDED_TO = "search_text_case_sended_to";

  public static final String INPUT_HIDDEN_MISSION_ID = "hidden_mission_id";
  public static final String INPUT_TEXT_MISSION_NAME = "text_mission_name";
  public static final String INPUT_TEXTAREA_MISSION_DESCRIPTION = "textarea_mission_description";
  public static final String INPUT_TEXT_MISSION_START_DATE = "text_mission_start_date";
  public static final String INPUT_TEXT_MISSION_END_DATE = "text_mission_end_date";
  public static final String INPUT_SELECT_FIL_SURVEY_ID = "select_fil_survey_id";
  public static final String INPUT_HIDDEN_OLD_MISSION_STATUS_ID = "hidden_old_mission_status_id";
  public static final String INPUT_SELECT_MISSION_STATUS_ID = "select_mission_status_id";
  public static final String INPUT_CHECKBOX_MISSION_USER_ID = "checkbox_mission_user_id";
  public static final String INPUT_TEXT_DCM_CODE = "text_dcm_code";
  public static final String INPUT_TEXT_STK_NUMBER = "text_stk_number";
  public static final String INPUT_HIDDEN_DCM_CATEGORY = "hidden_dcm_category";
  public static final String DCM_CATEGORY_SURVEYS_ALL = "dcm_category_surveys_all";
  public static final String DCM_CATEGORY_SURVEYS_COMPLETED = "dcm_category_surveys_completed";
  public static final String DCM_CATEGORY_SURVEYS_INCOMPLETED = "dcm_category_surveys_incompleted";
  public static final String INPUT_TEXT_SUPER_TYPE_NAME = "text_super_type_name";
  
  public static final String INPUT_SEARCH_TEXT_MISSION_NAME = "search_text_mission_name";
  public static final String INPUT_SEARCH_SELECT_MISSION_STATUS = "search_select_mission_status";
  public static final String INPUT_SEARCH_TEXT_MISSION_START_DATE_FROM = "search_text_mission_start_date_from";
  public static final String INPUT_SEARCH_TEXT_MISSION_START_DATE_TO = "search_text_mission_start_date_to";
  public static final String INPUT_SEARCH_TEXT_MISSION_END_DATE_FROM = "search_text_mission_end_date_from";
  public static final String INPUT_SEARCH_TEXT_MISSION_END_DATE_TO = "search_text_mission_end_date_to";

  public static final String MISSION_DETAILS_COMING_TABLE = "mission_details_coming_table";

  public static final String INPUT_HIDDEN_CASE_INFO_ELEMENT_ID = "input_hidden_case_info_element_id";
  public static final String INPUT_TEXT_CASE_INFO_ELEMENT_TITLE = "input_text_case_info_element_title";
  public static final String INPUT_TEXT_CASE_INFO_ELEMENT_DATE = "input_text_case_info_element_date";
  public static final String INPUT_SELECT_CASE_INFO_ELEMENT_TYPE_ID = "input_select_case_info_element_type_id";
  public static final String INPUT_TEXTAREA_CASE_INFO_ELEMENT_DESCRIPTION = "input_textarea_case_info_element_description";
  public static final String INPUT_TEXT_CONTACT_NAME = "input_text_contact_name";
  public static final String INPUT_TEXTAREA_CONTACT_INFORMATION = "input_textarea_contact_information";
  public static final String HLP_INPUT_KEY_ACTION = "input_key_action";
}