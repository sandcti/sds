package com.mobinil.sds.web.interfaces.sip;

public class SIPInterfaceKey
{

   //--------------------------Actions----------------------------------
      public static final String ACTION_SIP_FILE_DETAIL                    = "sip_file_detail";
   public static final String ACTION_SIP_FILE_DELETE                    = "sip_file_delete";
   
   
   //--------------------------Controls----------------------------------
      
   public static final String INPUT_HIDDEN_FILE_TYPE_ID                     = "file_type_id";
   public static final String INPUT_HIDDEN_FILE_ID                     = "file_id";
//   public static final String INPUT_HIDDEN_DELETE_SIP                   = "Delete";
   
   
   public static final String VECTOR_FILES                           = "vector_files";
   
   // update to interface key
   public static final String VECTOR_SIP_REPORT_YEAR                           = "vector_sip_report_year";
   
   public static final String VECTOR_SIP_ITEM_AMOUNT= "vector_sip_item_amount";
   
   

   
   //--------------------------Actions----------------------------------
   public static final String ACTION_ADD_NEW_LABEL                      = "add_new_label";
   public static final String ACTION_CREATE_NEW_SIP_REPORT              = "create_new_sip_report";
   public static final String ACTION_DELETE_ALL_LABEL_DETAILS           = "delete_all_label_details";
   public static final String ACTION_DELETE_SIP                         = "delete_sip";
   public static final String ACTION_DELETE_SIP_REPORT                  = "delete_sip_report";
   public static final String ACTION_DELETE_SIPREPORT                   = "delete_sip_report";   
   public static final String ACTION_DELETE_UPLOAD_FILE                 = "delete_upload_file";
   public static final String ACTION_DELETE_UPLOAD_FILE_PROCESS         = "delete_upload_file_process";
   public static final String ACTION_EDIT_SIP_REPORT                    = "edit_sip_report";
   public static final String ACTION_EXPORT_DATA                        = "export_data";
   public static final String ACTION_EXPORT_LABEL_DETAILS_DATA          = "export_label_details_data";
   public static final String ACTION_EXPORT_SIP_LIST                    = "export_sip_list";
   public static final String ACTION_EXPORT_SIP_TO_EXCEL                = "export_sip_to_excel";
   public static final String ACTION_HIDDEN_PARAM_NAME_PREVIOUS_ACTION  = "previous_action";   
   public static final String ACTION_HIDDEN_PARAM_NAME_SIP              = "sip_action";   
   public static final String ACTION_HIDDEN_PARAM_NAME_UPLOAD_DATA      = "upload_data_process";
   public static final String ACTION_SAVE_LABEL_DATA                    = "save_label_data";
   public static final String ACTION_SAVE_LABEL_DETAILS_DATA            = "save_label_details_data";
   public static final String ACTION_SAVE_NEW_SIP_REPORT                = "save_new_sip_report";
   public static final String ACTION_SAVE_NEW_SIP_REPORT_NO_PARAM       = "save_new_sip_report_no_param";   
   public static final String ACTION_SAVE_NEW_SIP_REPORT_PARAM          = "save_new_sip_report_param";   
   public static final String ACTION_SAVE_SIP_FACTORS                   = "save_sip_factors";   
   public static final String ACTION_SAVE_SIP_REPORT_TYPE               = "save_sip_report_type";   
   public static final String ACTION_SAVE_SIP_REPORT                    = "save_sip_report";   
   public static final String ACTION_SEARCH_DCM_PAYMENTS                = "search_dcm_payments";
   public static final String ACTION_SEARCH_PAYMENT                     = "search_payment";
   public static final String ACTION_SEARCH_SIP                         = "search_sip";   
   public static final String ACTION_CREATE_REPORT_TYPE					= "create_report_type";
   public static final String ACTION_EXPORT_FILE						= "export_file";
   public static final String ACTION_EXPORT_SAVED_REPORT_EXCEL			= "export_saved_report_excel";
   public static final String ACTION_SHOW_ALL_SIP_REPORTS               = "show_all_sip_reports";   
   public static final String ACTION_SHOW_LABEL_DATA                    = "show_label_data";
   public static final String ACTION_SIP_ADD_NEW_PAYMENT                = "sip_add_new_payment";
   public static final String ACTION_SIP_ADD_NEW_SIP                    = "sip_add_new_sip";
   public static final String ACTION_SIP_ASSIGN_CHANNEL_TO_USER         = "sip_assign_channels_to_user";   
   public static final String ACTION_SIP_CREATE_NEW_PAYMENT             = "create_new_payment";
   public static final String ACTION_SIP_CREATE_NEW_SIP                 = "create_new_sip";   
   public static final String ACTION_SIP_CREATE_NEW_SIP_CATEGORY        = "create_new_sip_category";   
   public static final String ACTION_SIP_EDIT_CHANNEL                   = "sip_edit_channels";
   public static final String ACTION_SIP_EDIT_PAYMENT                   = "sip_edit_payment";
   public static final String ACTION_SIP_EDIT_SIP                       = "sip_edit_sip";
   public static final String ACTION_SIP_EDIT_SIP_CATEGORY              = "edit_sip_category";
   public static final String ACTION_SIP_EXPORT_PAYMENT                 = "sip_export_payment";
   public static final String ACTION_SIP_FACTORS                        = "sip_factors";
   public static final String ACTION_SIP_INCOMING_ACTION                = "sip_incoming_action";
   public static final String ACTION_SIP_MANAGE_CATEGORY                = "sip_manage_category";
   public static final String ACTION_SIP_NEW_CHANNEL                    = "sip_new_channels";
   public static final String ACTION_SIP_SAVE_CHANNELS                  = "sip_save_channels";
   public static final String ACTION_SIP_SAVE_NEW_REPORT                = "sip_save_new_report";
   public static final String ACTION_SIP_SAVE_NEW_SIP                   = "sip_save_new_sip";   
   public static final String ACTION_SIP_SAVE_NEW_SIP_NO_PARAM          = "sip_save_new_sip_no_param";
   public static final String ACTION_SIP_SAVE_NEW_SIP_PARAM             = "sip_save_new_sip_param";
   public static final String ACTION_SIP_SAVE_SIP_CATEGORY              = "sip_save_sip_category";
   public static final String ACTION_SIP_SEARCH_PAYMENT                 = "sip_search_payment";
   public static final String ACTION_SIP_SEARCH_REPORT                  = "SIP_SEARCH_RERPORT";
   public static final String ACTION_SIP_SEARCH_SIP                     = "sip_search_sip";   
   public static final String ACTION_SIP_USER_CHANNELS                  = "sip_user_channels";   
   public static final String ACTION_SIP_VIEW_CHANNELS                  = "sip_view_channels";
   public static final String ACTION_SIP_VIEW_PAYMENT                   = "sip_view_sip";
   public static final String ACTION_SIPREPORT_CREATE_NEW_SIPREPORT     = "create_new_sip_report";   
   public static final String ACTION_UPDATE_LABEL_DATA                  = "update_label_data";   
   public static final String ACTION_UPDATE_SIP_REPORT                  = "update_sip_report";
   public static final String ACTION_UPDATE_SIP_STATUS                  = "update_sip_status";   
   public static final String ACTION_UPDATE_SIPREPORT_STATUS            = "update_sip_report_status";
   public static final String ACTION_UPLOAD_DIST_SUPERDEALERS           = "upload_dist_superdealers";
   public static final String ACTION_UPLOAD_DISTRIBUTORS_PER_EXECUTIVES = "upload_list_dist_exec";   
   public static final String ACTION_UPLOAD_EXPOS_PER_SUPERDEALER       = "upload_expos_superdealer";
//   public static final String ACTION_UPLOAD_FIELD_REP_SUPERVISOR        = "upload_field_rep_supervisor";
   public static final String ACTION_UPLOAD_FILES                       = "upload_files";
//   public static final String ACTION_UPLOAD_POS_FIELD_REP               = "upload_pos_field_rep";
//   public static final String ACTION_UPLOAD_SALES_SUPERVISOR            = "upload_sales_supervisor";   
   public static final String ACTION_UPLOAD_SIP_DATA                    = "upload_sip_data";   
   public static final String ACTION_UPLOAD_SIP_DATA_PROCESS            = "upload_sip_data_process";
   public static final String ACTION_VIEW_FINAL_SIP                    = "view_final_sip";
   public static final String ACTION_VIEW_DCM_PAYMENTS                  = "view_dcm_payments";
   public static final String ACTION_VIEW_LABEL_DETAILS_DATA            = "view_label_details_data";
   public static final String ACTION_VIEW_PREPARING_SIP                 = "view_preparing_sip";
   public static final String ACTION_VIEW_READY_SIP                     = "view_ready_sip";
   public static final String ACTION_VIEW_SIP                           = "view_sip";
   public static final String ACTION_VIEW_SIP_LABELS                    = "view_sip_labels";
   public static final String ACTION_SIP_NEW_CONFIG                     = "sip_new_config";
   public static final String ACTION_SIP_SAVE_CONFIG                    = "sip_save_config";
   public static final String ACTION_SIP_VIEW_CONFIG                    = "sip_view_config_screen";
   public static final String ACTION_SIP_EDIT_CONFIG                    = "sip_edit_config";
   public static final String ACTION_SIP_DELETE_CHANNEL					= "sip_delete_channel";
   public static final String ACTION_SIP_DELETE_CONFIG 					="sip_delete_config";
  
   public static final String ACTION_SIP_NEW_SAVED_REPORT               = "sip_new_saved_report";
   public static final String ACTION_SIP_SAVE_SAVED_REPORT              = "sip_save_saved_report";
   public static final String ACTION_SIP_VIEW_SAVED_REPORT              = "sip_view_saved_report";
   public static final String ACTION_SIP_EDIT_SAVED_REPORT              = "sip_edit_saved_report";
   public static final String ACTION_SIP_DELETE_SAVED_REPORT 		    = "sip_delete_saved_report";
   
   public static final String ACTION_UPLOAD_SALES_SUPERVISOR            = "upload_sales_supervisor";
   public static final String ACTION_UPLOAD_EXPOS_SUPERDEALER            = "upload_expos_superdealer";
   public static final String ACTION_UPLOAD_DCM_EXPOS            = "upload_dcm_expos";
   public static final String ACTION_UPLOAD_FIELD_REP_SUPERVISOR            = "upload_field_rep_supervisor";
   public static final String ACTION_UPLOAD_POS_FIELD_REP            = "upload_pos_field_rep";
   public static final String ACTION_UPLOAD_LIST_DIST_EXEC            = "upload_list_dist_exec";
public static final String ACTION_SEARCH_FILE						="search_file";
   public static final String ACTION_SIP_CHANGE_STATUS            = "sip_change_status";
   
   
   public static final String  ACTION_SAVE_NEW_SIP_REPORT_1  ="save_new_sip_report_1";
   
   public static final String   ACTION_SEARCH_SAVED_REPORT="search_saved_report";
   
   public static final String   ACTION_FACTOR_EDIT = "sip_factors_edit";
   public static final String   ACTION_FACTOR_UPDATE = "sip_factors_update";
   
   public static final String   ACTION_EXPORT_SAVED_REPORT ="export_saved_report";
   
   
   //--------------------------Controls----------------------------------
   public static final String CHANNEL_MODEL                             = "channel_model";
   public static final String CONFIG_MODEL								= "config_model";
   public static final String SIP_REPORT_EDITTED_MODEL                              = "sip_report_editted_model";
   public static final String SAVED_REPORT_MODEL						= "saved_report_model";
   public static final String CHANNEL_VECTOR                            = "channel_vec";
   public static final String CONFIG_VECTOR                             = "config_vec";
   public static final String SAVED_REPORT_VECTOR						="saved_report_vector";
   public static final String CONTROL_HIDDEN_CHANNEL_ID                 = "channel_id";
   public static final String CONTROL_HIDDEN_REPORT_TYPE_ID             = "hidden_report_type_id";
   public static final String CONTROL_TEXT_CHANNEL_NAME                 = "channel_NAME";
   public static final String CONTROL_TEXT_SIP_BASE                     = "sip_base";
   public static final String CONTROL_TEXT_SIP_CATEGORY                 = "sip_category";
   public static final String CONTROL_TEXT_SIP_CATEGORY_DESC            = "sip_category_desc";
   public static final String CONTROL_TEXT_SIP_CATEGORY_NAME            = "sip_category_name";
   public static final String CONTROL_TEXT_SIP_CHANNEL                  = "sip_channel";   
   public static final String CONTROL_TEXT_SIP_DATA_VIEW                = "sip_data_view";
   public static final String CONTROL_TEXT_SIP_DATA_VIEW_TYPE           = "sip_data_view_type";
   public static final String CONTROL_TEXT_SIP_DESCRIPTION              = "sip_description";
   public static final String CONTROL_TEXT_SIP_END_DATE_FROM            = "sip_end_date_from";
   public static final String CONTROL_TEXT_SIP_END_DATE_TO              = "sip_end_date_to";
   public static final String CONTROL_TEXT_SIP_FACTOR_NAME              = "sip_factor_name";
   public static final String CONTROL_TEXT_SIP_FACTOR_VALUE             = "sip_factor_value";
   public static final String CONTROL_TEXT_SIP_ID                       = "sip_id";
   public static final String CONTROL_TEXT_SIP_NAME                     = "sip_name";
   public static final String CONTROL_TEXT_SIP_REPORT_CATEGORY          = "control_text_sip_report_category";
   public static final String CONTROL_TEXT_SIP_REPORT_CHANNEL           = "control_text_sip_report_channel";
   public static final String CONTROL_TEXT_SIP_REPORT_DATA_VIEW         = "sipReport_data_view";
   public static final String CONTROL_TEXT_SIP_REPORT_DATA_VIEW_TYPE    = "sipReport_data_view_type";
   public static final String CONTROL_TEXT_SIP_REPORT_DESCRIPTION       = "sipReport_description";
   public static final String CONTROL_TEXT_SIP_REPORT_END_DATE          = "sip_reprot_end_date";
   public static final String CONTROL_TEXT_SIP_REPORT_END_DATE_FROM     = "control_text_sip_report_end_date_from";
   public static final String CONTROL_TEXT_SIP_REPORT_END_DATE_TO       = "control_text_sip_report_end_date_to";
   public static final String CONTROL_TEXT_SIP_REPORT_ID                = "control_text_sip_report_id";
   public static final String CONTROL_TEXT_SIP_REPORT_NAME              = "control_text_sip_report_name";
   public static final String CONTROL_TEXT_SIP_REPORT_START_DATE        = "sip_reprot_start_date";
   public static final String CONTROL_TEXT_SIP_REPORT_START_DATE_FROM   = "control_text_sip_report_start_date_from";
   public static final String CONTROL_TEXT_SIP_REPORT_START_DATE_TO     = "control_text_sip_report_start_date_to";
   public static final String CONTROL_TEXT_SIP_REPORT_STATUS            = "control_text_sip_report_status";
   public static final String CONTROL_TEXT_SIP_REPORT_TYPE              = "control_text_sip_report_type";
   public static final String CONTROL_TEXT_SIP_START_DATE_FROM          = "sip_start_date_from";
   public static final String CONTROL_TEXT_SIP_START_DATE_TO            = "sip_start_date_to";
   public static final String CONTROL_TEXT_SIP_STATUS                   = "sip_status";
   public static final String CONTROL_SELECT_SIP_QUARTER                   = "select_sip_quarter";
   public static final String CONTROL_SELECT_SIP_LABEL                   = "select_sip_label";
   public static final String CONTROL_SELECT_SIP_CAT_TYPE                   = "select_sip_cat_type";
   public static final String CONTROL_TEXT_SIP_SUBTRACT_ID              = "subtractcomId";
   public static final String CONTROL_TEXT_SIP_TYPE                     = "sip_type";
   public static final String CONTROL_TEXT_SIPREPORT_END_DATE           = "sipreport_end_date";
   public static final String CONTROL_TEXT_SIPREPORT_START_DATE         = "sipreport_start_date";
   public static final String CONTROL_HIDDEN_CONFIG_ID					="hidden_config_id";
   public static final String CONTROL_TEXT_CONFIG_NAME					="text_config_name";
   public static final String CONTROL_SELECT_CONFIG_TYPE				="select_config_type";
   public static final String CONTROL_HIDDEN_SAVED_REPORT_ID			="hidden_saved_report_id";
   public static final String CONTROL_HIDDEN_SAVED_REPORT_TYPE_ID			="hidden_saved_report_type_id";
  public static final String CONTROL_HIDDEN_FILE_TYPE_ID				="hidden_file_type_id";
   public static final String CONTROL_TEXT_SAVED_REPORT_NAME			="text_saved_report_name";
   public static final String CONTROL_SELECT_SAVED_REPORT_TYPE			="select_saved_report_type";
   public static final String DATA_VIEW_TYPE_A                          = "cross_tab";
   public static final String DATA_VIEW_TYPE_B                          = "normal";
   public static final String FILE_EXPORT                               = "file_export";
   public static final String SIP_REPORT_ERROR_MESSAGE                       = "sip_error_message";
   public static final String SIP_REPORT_STRING_ERROR_MESSAGE                       
   = "Sorry, this report cannot be final due to there are another report is final with the same quartar and channal.";
   
   public static final String INPUT_TEXT_SIP_YEAR                      = "sip_year";
   public static final String INPUT_TEXT_SIP_MONTH                      = "sip_month";
   public static final String INPUT_TEXT_LINE_COMMISSION_IDS                      = "line_commission_ids";
   public static final String INPUT_TEXT_SIP_REPORT_NAME                      = "sip_report_name";
   public static final String INPUT_TEXT_NI_COMMISSION_IDS                      = "ni_commission_ids";
   public static final String INPUT_TEXT_SOP_IDS                      = "sop_ids";
   public static final String HIDDEN_PARAM_NAME_SIP_REPORT              = "sip_report_name";
   public static final int    HIDDEN_PARAM_SIP_REPORT_DCM               = 1;
   public static final int    HIDDEN_PARAM_SIP_REPORT_POS               = 5;
   public static final int    HIDDEN_PARAM_SIP_REPORT_EMP               = 7;
   public static final int    HIDDEN_PARAM_SIP_REPORT_EXPOS               = 6;
   public static final int    HIDDEN_PARAM_SIP_REPORT_REGION            = 2;
   public static final int    HIDDEN_PARAM_SIP_REPORT_SD                = 3;
   public static final int    HIDDEN_PARAM_SIP_REPORT_SUPERVISOR        = 4;
//   public static final String INPUT_HIDDEN_ALL_SIP                      = "input_hidden_all_sip";
   public static final String INPUT_HIDDEN_SIP_CAT_ID                      = "sip_cat_id";
   public static final String INPUT_HIDDEN_ALL_SIP                      = "All";
   public static final String INPUT_HIDDEN_ALL_SIP_REPORT               = "input_hidden_all_sip_report";
   public static final String INPUT_HIDDEN_CATEGORY_ID                  = "hidden_category_id";
   public static final String INPUT_HIDDEN_FINAL_SIP                    = "Final";
   public static final String INPUT_HIDDEN_DCM_CODE                     = "hidden_dcm_code";
   public static final String INPUT_HIDDEN_SIP_CHANGE_STATUS                     = "change_status";
   public static final String CONTROL_INCOMING_ACITON					="control_incoming_action";
   public static final String VECTOR_MEMO_REPORT_ERROR_MSG_TOBEVIEWED	="vector_memo_report_error_msg_topviewed";
   //   public static final String INPUT_HIDDEN_SIP_CAT_ID					="";
   
   //   public static final String INPUT_HIDDEN_DELETE_SIP                   = "Delete";
   
   public static final String INPUT_HIDDEN_EDIT_SIP                     = "Edit";
   public static final String INPUT_HIDDEN_LABEL_ID                     = "hidden_label_id";
   public static final String INPUT_HIDDEN_PREPAIRING_SIP                     = "Prepairing";
   public static final String INPUT_HIDDEN_READY_SIP                      = "Draft";
//   public static final String INPUT_HIDDEN_SIP_ACTION                   = "input_hidden_sip_action";
   public static final String INPUT_HIDDEN_SIP_ACTION                   = "hidden_sip_action";
   public static final String INPUT_HIDDEN_SIP_ID                       = "hidden_sip_id";
   public static final String INPUT_HIDDEN_SIP_REPORT_ACTION            = "input_hidden_sip_report_action";
   public static final String INPUT_HIDDEN_SIP_REPORT_ID                = "input_hidden_sip_report_id";
   public static final String INPUT_HIDDEN_SIP_REPORT_STATUS            = "input_hidden_sip_report_status";
   public static final String INPUT_HIDDEN_SIP_REPORT_CHANNEL_ID                   = "hidden_sip_report_channel_id";
   public static final String INPUT_HIDDEN_SIP_REPORT_QUARTAR_ID                   = "hidden_sip_report_quartar_id";
   public static final String INPUT_HIDDEN_SIP_REPORT_YEAR_ID                   = "hidden_sip_report_year_id";
   public static final String INPUT_HIDDEN_SIP_REPORT_LABEL_ID                   = "hidden_sip_report_label_id";
   // ///////////////////////////
   public static final String INPUT_HIDDEN_SIP_REPORT_TYPE_ID           = "sip_report_type_id";
//   public static final String INPUT_HIDDEN_SIP_STATUS                   = "input_hidden_sip_status";
   public static final String INPUT_HIDDEN_SIP_STATUS                   = "hidden_sip_status";
   public static final String INPUT_HIDDEN_SIP_TYPE_ID                  = "hidden_sip_type_id";
   public static final String INPUT_HIDDEN_TABLE_ID                     = "hidden_table_id";
   public static final String INPUT_HIDDEN_TYPE_ID                      = "hidden_type_id";
   public static final String INPUT_TEXT_DCM_CODE                       = "text_dcm_code";
   public static final String INPUT_TEXT_LABEL_DESCRIPTION              = "label_description";
   public static final String INPUT_TEXT_LABEL_NAME                     = "label_name";
   public static final String INPUT_TEXT_REPORT_NAME                    = "report_name";
   public static final String INPUT_TEXT_SIP_REPORT_ID                  = "sip_report_id_text";
   public static final String MODEL_EDIT_CATEGORY_MODEL                 = "edit_category_model";
   public static final String SAVED_SIP_ID                              = "SAVED_SIP_ID";
   public static final String SAVED_SIP_REPORT_ID                       = "saved_sip_report_id";
   public static final String SIP_DATA_VIEW_PARAMETER                   = "sip_data_view_parameter";
   public static final String INPUT_SEARCH_YEAR							= "search_year";
   public static final String INPUT_SEARCH_QUARTER						= "search_quarter";
   public static final String INPUT_SEARCH_FILE_TYPE					= "search_file_type";
   public static final String USER_CHANNEL_VECTOR                       = "user_channel_vec";
   public static final String VEC_SIP_LISTS                             = "vec_sip_lists";
   
   public static final String VECTOR_PAYMENT_STATUS                     = "vector_payment_status";
   // ///////////
   
   public static final String FACTOR_OPTIONS = "sip_report_factor_options";
   public static final String VECTOR_SCHEMA_SIP                         = "vector_schema_sip";
   public static final String VECTOR_SIP_FACTORS                        = "vector_sip_factors";
   public static final String VECTOR_SIP_REPORT_SEARCH_RESULT           = "VECTOR_SIP_REPORT_SEARCH_RESULT";
   public static final String VECTOR_SIP_REPORT_STATUS                  = "vector_sip_report_status";
   public static final String VECTOR_SIP_REPORT_TYPE                    = "vector_sipReport_type";
   public static final String VECTOR_SIP_REPORT_TYPE_CATEGORY           = "vector_report_type_category";
   public static final String VECTOR_SIP_SEARCH_RESULT                  = "vector_sip_search_result";
   public static final String VECTOR_SIP_STATUS                         = "vector_sip_status";
   public static final String VECTOR_SIP_LABEL                         = "vector_sip_labels";
   public static final String VECTOR_SIP_QUARTER                         = "vector_sip_quarter";
   public static final String VECTOR_SIP_TYPE                           = "vector_sip_type";
   public static final String VECTOR_SIP_YEAR							="vector_sip_year";
   public static final String VECTOR_SIP_TYPE_CATEGORY                  = "vector_sip_type_category";
   public static final String SIP_REPORT_DATA_VIEW_PARAMETER="sip_report_data_view_parameter";
   
   //---------------------------------------Sip report Status----------------------------------------------
   public static final String SIP_REPORT_STATUS_READY                       = "2";
   public static final String SIP_REPORT_STATUS_FINAL                       = "3";
   public static final String SIP_REPORT_STATUS_DELETED                       = "5";
   public static final String SIP_REPORT_STATUS_PREPARING                       = "1";
   
   
   public static final String SIP_REPORT_FACTOR_VALUE_INCLUDE                       = "1";
   public static final String SIP_REPORT_FACTOR_VALUE_EXCLUDE                       = "0";
   
   
   public static final String SIP_REPORT_QUARTAR_1                       = "1,2,3";
   public static final String SIP_REPORT_QUARTAR_2                       = "4,5,6";
   public static final String SIP_REPORT_QUARTAR_3                       = "7,8,9";
   public static final String SIP_REPORT_QUARTAR_4                       = "10,11,12";
   
   public static final String HASHMAP_KEY_SEARCH_USER_ID                       = "filterByUserID";
   
}
