package com.mobinil.sds.web.interfaces.aacm;

public class AACMInterfaceKey {
	
	public static final String ACTION_VIEW_AUTHORIZED_AGENT = "view_authorized_agent";
	public static final String ACTION_CREATE_NEW_AUTHAGENT = "create_new_authAgent";
	public static final String ACTION_EDIT_AUTHAGENT = "edit_authAgent";
	public static final String ACTION_SAVE_NEW_AUTHAGENT = "save_new_authAgent";
	public static final String ACTION_DELETE_AUTHAGENT = "delete_authAgent";
	public static final String ACTION_UPDATE_AUTHAGENT = "update_authAgent";
	public static final String ACTION_VIEW_AMS_DATA = "view_ams_data";
	public static final String ACTION_IMPORT_AMS_DATA = "import_ams_data";
	public static final String ACTION_AUTHAGENT_DATA_IMPORT = "authAgent_data_import";
	public static final String ACTION_AUTHAGENT_DATA_IMPORT_PROCESS = "authAgent_data_import_process";
	public static final String ACTION_UPLOAD_AMS_DATA ="upload_ams_data";
	public static final String ACTION_UPLOAD_AMS_DATA_PROCESS = "upload_ams_data_process";
	public static final String ACTION_VIEW_AMS_DATA_FILE = "view_ams_data_file";
	public static final String ACTION_DELETE_AMS_DATA_FILE = "delete_ams_data_file";
	public static final String ACTION_DELETE_AUTH_AGENT_FILE = "delete_auth_agent_file";
	public static final String ACTION_VIEW_AUTH_AGENT_FILE = "view_auth_agent_file";
	public static final String ACTION_EXPORT_AMS_DATA_TO_EXCEL = "export_ams_data_to_excel";
	public static final String ACTION_EXPORT_AUTH_AGENT_DATA_TO_EXCEL = "export_auth_agent_data_to_excel";
	
	
	public static final String INPUT_TEXT_AUTHAGENT_CODE = "text_authAgent_code";
	public static final String INPUT_TEXT_AUTHAGENT_NAME = "text_authAgent_name";
	public static final String INPUT_HIDDEN_DCM_ID = "hidden_dcm_id";
	public static final String INPUT_HIDDEN_AUTHAGENT_NAME = "hidden_authagent_name";
	public static final String INPUT_HIDDEN_AUTHAGENT_CODE = "hidden_authagent_code";
	public static final String INPUT_HIDDEN_CHANNEL_ID = "hidden_channel_id";
	public static final String INPUT_HIDDEN_AMS_FILE_ID = "hidden_ams_file_id";
	public static final String INPUT_HIDDEN_AUTH_AGENT_FILE_ID = "hidden_auth_agent_file_id";
	public static final String CONST_GEN_CHANNEL_AUTHORIZED_AGENT = "4";
	public static final String CONTROL_SELECT_IMPORT_AMS_YEAR = "control_select_import_ams_year";
	public static final String CONTROL_SELECT_IMPORT_AMS_MONTH = "control_select_import_ams_month";

        
        
        public static final String CONTROL_SELECT_YEAR_NAME="year_name";
    public static final String CONTROL_SELECT_MONTH_NAME="month_name";
    public static final String CONTROL_FILE_REVENUE_FILE_UPLOAD="upload";
    
    public static final String ACTION_UPLOAD_REV_FILE="upload_rev_process";
    public static final String ACTION_MANAGEMENT_REV_FILE="management_rev_file";
    public static final String ACTION_EXPORT_REV_FILE="export_rev_file";
    public static final String ACTION_DELETE_REV_FILE="delete_rev_file";
    public static final String ACTION_UPLOAD_REVENUE_FILE_PROCESS="upload_rev_file_process";
    public static final String ERROR_MSG="msg";
    public static final String ERROR_VECTOR="error_vec";
    public static final String VECTOR_OF_FILES="revenue_file_vec";
    public static final String CONTROL_HIDDEN_FILE_ID_UPLOAD="hidden_file_id";
        public static final String EXPORT_FILE_REVENUE_PATH="export_rev_file_path";
    
}
