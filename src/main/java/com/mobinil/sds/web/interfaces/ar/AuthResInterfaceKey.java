package com.mobinil.sds.web.interfaces.ar;

public class AuthResInterfaceKey {
	
	
	

	
	public static final String ACTION_VIEW_LABEL_MANAGEMENT = "view_label_management";
	public static final String ACTION_VIEW_SEARCH_CATEGORY_MANAGEMENT = "view_search_category_management";
	
	public static final String ACTION_CREATE_NEW_LABEL = "create_new_label";
	public static final String ACTION_CREATE_NEW_SEARCH_CATEGORY = "create_new_search_category";
	
	public static final String ACTION_SAVE_NEW_LABEL = "save_new_label";
	public static final String ACTION_SAVE_NEW_SEARCH_CATEGORY = "save_new_search_category";
	
	public static final String ACTION_DELETE_LABEL = "delete_label";	
	public static final String ACTION_DELETE_SEARCH_CATEGORY = "delete_search_category";
	
	public static final String ACTION_EDIT_LABEL = "edit_label";
	public static final String ACTION_EDIT_SEARCH_CATEGORY = "edit_search_category";
	
	
	public static final String ACTION_UPDATE_LABEL = "update_label";
	public static final String ACTION_UPDATE_SEARCH_CATEGORY = "update_search_category";
	
	
	public static final String ACTION_AUTH_RES_IMPORT = "auth_res_import";
	public static final String ACTION_SIM_INFO_IMPORT = "sim_info_import";
	public static final String ACTION_DOWNLOAD_SIM_INFO_RESULT_FILE = "action_download_sim_info_result_file";
	
	public static final String ACTION_AUTH_RES_IMPORT_PROCESS = "auth_res_import_process";
	public static final String ACTION_SIM_INFO_IMPORT_PROCESS = "sim_info_import_process";
	
	public static final String ACTION_VIEW_AUTH_RES_FILE = "view_auth_res_file";	
	public static final String ACTION_VIEW_AUTH_SIM_INFO_FILE = "view_auth_sim_info_file";	
	public static final String ACTION_DELETE_AUTH_SIM_INFO_FILE = "delete_auth_sim_info_file";	
	public static final String ACTION_DELETE_AUTH_RES_FILE = "delete_auth_res_file";
	public static final String ACTION_VIEW_STATISTICS = "view_statistics";
	public static final String ACTION_AUTH_RES_SEARCH_IMPORT_PROCESS= "auth_res_search_import_process";
	public static final String ACTION_AUTH_RES_SEARCH_IMPORT= "auth_res_search_import";
	public static final String ACTION_DELETE_AUTH_SEARCH_RES_FILE= "delete_auth_search_res_file";
	public static final String ACTION_VIEW_AUTH_RES_SEARCH_FILE= "view_auth_res_search_file";
	public static final String 	ACTION_VIEW_SEARCH_FILE_DATA= "view_search_file_data";
	public static final String ACTION_VIEW_SEARCH_FILE_INVALID_SIMS= "view_search_file_invalid_sims";
	public static final String ACTION_VIEW_USER_LABEL = "view_user_label";
	public static final String ACTION_VIEW_LABEL_LIST = "view_label_list";
	public static final String ACTION_ADD_NEW_USER = "add_new_user";
	public static final String ACTION_SAVE_NEW_USER = "save_new_user";
	public static final String ACTION_ASSIGN_LABEL = "assign_label";
	public static final String ACTION_SAVE_ASSIGNED_LABEL = "save_assigned_label";
	public static final String ACTION_DELETE_USER = "delete_user";
	public static final String ACTION_DELETE_USER_LABEL = "delete_user_label";
	public static final String ACTION_ASSIGN_LABEL_TO_USER = "assign_label_to_user";
	public static final String ACTION_UPDATE_USER_LABELS= "update_user_labels";
	
	 public static final String VECTOR_LABEL = "vector_label";
	 public static final String VECTOR_SEARCH_CATEGORY = "vector_search_category";
	 public static final String VECTOR_FILES = "vector_files";
	 public static final String VECTOR_STATISTICS = "vector_statistics";
     public static final String  VECTOR_SEARCH_FILES ="vector_search_files";
     public static final String  VECTOR_SEARCH_FILES_DATA="vector_search_files_data";
     public static final String  VECTOR_SEARCH_FILES_INVALID_SIM="vector_search_files_invalid_sim";
   
   
	
	
	
	
     public static final String CONTROL_HIDDEN_LABEL_ID = "control_hidden_label_id";
     public static final String FILE_EXT = "file_ext";
     public static final String CONTROL_SELECT_SEARCH_CATEGORY_ID = "control_select_search_cat_id";
     public static final String CONTROL_SELECT_FILE_TYPE_ID = "control_select_file_type_id";
     public static final String CONTROL_COMBO_SEARCH_BY_ID = "control_combo_search_by_id";
     public static final String CONTROL_HIDDEN_LABEL_NAME = "control_hidden_label_name";
     public static final String CONTROL_SELECT_YEAR = "control_select_year";
     public static final String CONTROL_SELECT_MONTH = "control_select_month";
     public static final String    CONTROL_INPUT_DESCRIPTION = "control_input_description";
     public static final String     CONTROL_HIDDEN_FILE_ID= "control_hidden_file_id";
     
    
    
    
     public static final String     CONTROL_SELECT_OPTION_SIM_ONLY= "1";
     public static final String     CONTROL_SELECT_OPTION_SIM_DATA= "2";
    
    
    
	
	
	
	
	
	public static final String INPUT_TEXT_LABEL_NAME = "input_text_label_name";
	
	public static final String INPUT_TEXT_LABEL_DESCRIPTION = "input_text_label_description";
	
	public static final String INPUT_TEXT_CATEGROY_NAME = "input_text_category_name";
	public static final String INPUT_TEXT_CATEGORY_DESCRIPTION = "input_text_category_description";
	
	public static final String INPUT_HIDDEN_label_ID = "hidden_label_id";
	public static final String INPUT_HIDDEN_SEARCH_CATEGORY_ID = "hidden_search_category_id";
	public static final String INPUT_HIDDEN_USER_ID = "user_id";
	public static final String INPUT_TEXT_USER_ID = "text_user_id";

	public static final String VALID_IP = "127.0.0.1";

	public static final String STATUS_PROCESSING = "processing";
	public static final String FILE_ID = "fieldId";
	public static final String SIM_INFO_FILE_DOWNLOAD_PATH = "sim_info_file_download_path";
        
        public static final String ACTION_UPLOAD_VOC_MNP_MIGRA_PROCESS="upload_voc_mnp_migra_process";
        public static final String ACTION_UPLOAD_DATA_LINE_PROCESS="upload_data_line_process";
        public static final String ACTION_UPLOAD_ACHIV_PREPAID_PROCESS="upload_achiv_prepaid_process";
        public static final String ACTION_GROSS_ADDS_MANAGEMENT="action_gross_adds_stat";
        public static final String ACTION_GROSS_ADDS_FILE_DELETE="action_gross_adds_delete_file";
        
        
        public static final String CONTROL_SELECT_MONTH_NUMBER="control_select_month_number";
        public static final String CONTROL_SELECT_YEAR_NUMBER="control_select_year_number";
        public static final String CONTROL_HIDDEN_UPLOAD_FILE_TYPE_ID="control_hidden_upload_file_type_id";
        public static final String CONTROL_HIDDEN_DELETE_FILE_ID="control_hidden_delete_file_id";
        public static final String CONTROL_SELECT_LABEL_ID="control_select_label_id";
        public static final String CONTROL_FILE_UPLOAD="control_file_upload";
        public static final String ERROR_MESSAGE="error_message";
        public static final String VECTOR_OF_ALL_GROSS_ADDS_FILES="vec_of_all_gross_adds_files";
        
        public static final String AUTH_UPLOAD_DIR="/authRes/uploadtext";
        
        
	
	
	
	
	
	
	
	
			
}
