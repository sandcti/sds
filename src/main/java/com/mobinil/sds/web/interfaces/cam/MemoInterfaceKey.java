package com.mobinil.sds.web.interfaces.cam;

public interface MemoInterfaceKey {

    public static final String ACTION_VIEW_MEMO = "view_memo";
    public static final String ACTION_VIEW_CLOSED_PAYMENTS = "view_closed_payments";
    public static final String ACTION_VIEW_FROZEN_CLOSED_PAYMENTS = "view_frozen_closed_payments";
    public static final String ACTION_VIEW_READY_MEMOS = "view_ready_memos";
    public static final String ACTION_VIEW_ISSUED_MEMOS = "view_issued_memos";
    public static final String ACTION_CREATE_NEW_MEMO = "create_new_memo";
    public static final String ACTION_CREATE_SPECIFIC_MEMO = "create_specific_memo";
    public static final String ACTION_CREATE_COLLECTION_MEMO = "create_collection_memo";
    public static final String ACTION_SEARCH_MEMO = "search_memo";
    public static final String ACTION_SEARCH_SPECIFIC_MEMO = "search_specific_memo";
    public static final String ACTION_SEARCH_COLLECTION_MEMO = "search_collection_memo";
    public static final String ACTION_SEARCH_SPECIFIC_COLLECTION_MEMO = "search_specific_collection_memo";
    public static final String ACTION_VIEW_MEMO_HISTORY = "view_memo_history";
    public static final String ACTION_SEARCH_MEMO_HISTORY = "search_memo_history";
    public static final String ACTION_VIEW_UPDATE_MEMO_HISTORY = "view_update_memo_history";
    public static final String ACTION_UPDATE_MEMO_HISTORY = "update_memo_history";
    public static final String ACTION_SEARCH_READY_MEMOS = "search_ready_memos";
    public static final String ACTION_SEARCH_ISSUED_MEMOS = "search_issued_memos";
    public static final String ACTION_DELETE_MEMO = "delete_memo";
    public static final String ACTION_DELETE_READY_MEMO = "delete_ready_memo";
    public static final String ACTION_VIEW_EDIT_READY_MEMO = "view_edit_ready_memo";
    public static final String ACTION_EDIT_READY_MEMO = "edit_ready_memo";
    public static final String ACTION_DELETE_ISSUED_MEMO = "delete_issued_memo";
    public static final String ACTION_GET_MEMO_REPORT = "get_memo_report";
    public static final String ACTION_SEARCH_IN_MEMO = "search_in_memo";
    public static final String ACTION_SET_NEGATIVE_TO_ZERO_IN_MEMO = "set_negative_to_zero_in_memo";
    public static final String ACTION_EXPORT_MEMO = "export_memo";
    public static final String ACTION_EXPORT_MEMO_TO_EXCEL = "export_memo_to_excel";
    public static final String ACTION_VIEW_DCM_DETAILS = "view_dcm_details";
    public static final String ACTION_SEARCH_DCM_DETAILS = "search_dcm_details";
    public static final String ACTION_VIEW_GEN_DCM_DETAILS = "view_gen_dcm_details";
    public static final String ACTION_SEARCH_GEN_DCM_DETAILS = "search_gen_dcm_details";
    public static final String ACTION_VIEW_SQL_TEMPLATE_MANAGEMENT = "view_sql_template_management";
    public static final String ACTION_VIEW_EDIT_SQL_TEMPLATE_MANAGEMENT = "view_edit_sql_template_management";
    public static final String ACTION_UPDATE_SQL_TEMPLATE_MANAGEMENT = "update_sql_template_management";
    public static final String VECTOR_SQL_TEMPLATES = "sql_templates";
    public static final String SQL_TEMPLATE_ID = "sql_template_id";
    public static final String HIDDEN_SQL_TEMPLATE_ID = "hidden_sql_template_id";
    public static final String SQL_TEMPLATE_SQL = "sql_template_sql";
    public static final String ACTION_VIEW_SQL_TABLE_BKP_MANAGEMENT = "view_sql_table_bkp_management";
    public static final String ACTION_CREATE_SQL_TABLE_BKP_FILE = "create_sql_table_bkp_file";
    public static final String SQL_TABLE_ID = "sql_table_id";
    public static final String ACTION_EXPORT_DCM_DETAILS_EXCEL = "export_dcm_details_excel";
    public static final String ACTION_EXPORT_GEN_DCM_DETAILS_EXCEL = "export_gen_dcm_details_excel";
    public static final String ACTION_EXPORT_ALL_DCM_DETAILS_EXCEL = "export_all_dcm_details_excel";
    public static final String ACTION_SEARCH_CLOSED_PAYMENTS = "search_closed_payments";
    public static final String ACTION_SEARCH_FROZEN_CLOSED_PAYMENTS = "search_frozen_closed_payments";
    public static final String ACTION_VIEW_PAYMENT_CONTENTS = "view_payment_contents";
    public static final String ACTION_VIEW_FROZEN_PAYMENT_CONTENTS = "view_frozen_payment_contents";
    public static final String ACTION_FREEEZE_PAYMENT = "freeze_payment";
    public static final String ACTION_FREEEZE_DCM = "freeze_DCM";
    public static final String ACTION_EXPORT_DCM_PAYMENT_MEMBERS_EXCEL = "export_dcm_payment_members_excel";
    public static final String ACTION_GENERATE_SEARCH_DCM_DETAILS_LIST_TEMPLATE = "generate_search_dcm_details_list";
    public static final String ACTION_VIEW_UPLOAD_SEARCH_DCM_DETAILS_LIST = "view_upload_search_dcm_details_list";
    public static final String ACTION_UPLOAD_SEARCH_DCM_DETAILS_LIST = "upload_search_dcm_details_list";
    public static final String ACTION_VIEW_RECENTLY_UPLOADED_SEARCH_LIST = "view_recently_upoladed_search_list";
    public static final String ACTION_GET_MEMO_REASONS = "get_memo_reasons";
    public static final String ACTION_UPDATE_MEMO_REASON = "update_memo_reason";
    public static final String ACTION_VIEW_UPDATE_MEMO_REASON = "view_update_memo_reason";
    public static final String ACTION_ADD_MEMO_REASON = "add_memo_reason";
    public static final String ACTION_VIEW_ADD_MEMO_REASON = "view_add_memo_reason";
    public static final String ACTION_DELETE_MEMO_REASON = "delete_memo_reason";
    public static final String ACTION_VIEW_MEMO_DELAY_REASONS = "view_memo_delay_reasons";
    public static final String ACTION_UPDATE_MEMO_DELAY_REASON = "update_memo_delay_reason";
    public static final String ACTION_VIEW_UPDATE_MEMO_DELAY_REASON = "view_update_memo_delay_reason";
    public static final String ACTION_ADD_MEMO_DELAY_REASON = "add_memo_delay_reason";
    public static final String ACTION_VIEW_ADD_MEMO_DELAY_REASON = "view_add_memo_delay_reason";
    public static final String ACTION_DELETE_MEMO_DELAY_REASON = "delete_memo_delay_reason";
    public static final String ACTION_GENERATE_MEMO_REMOVAL_TEMPLATE = "generate_memo_removal_template";
    public static final String ACTION_IMPORT_MEMO_REMOVAL_FILE = "import_memo_removal_file";
    public static final String ACTION_IMPORT_MEMO_REMOVAL_FILE_PROCESS = "import_memo_removal_file_process";
    public static final String ACTION_VIEW_IMPORT_MEMO_REMOVAL = "view_import_memo_removal";
    //public static final String  ACTION_VIEW_MEMO_REMOVAL_REASONS="view_memo_removal_reasons";
    public static final String ACTION_ISSUE_MEMO = "issue_memo";
    public static final String ACTION_ISSUE_READY_MEMO = "issue_ready_memo";
    public static final String ACTION_FINANCE_ISSUE_MEMO = "finance_issue_memo";
    public static final String ACTION_REMOVE_MEMBERS_OF_MEMO = "delete_members_of_memo";
    public static final String ACTION_VIEW_MEMO_EXCEL_MANAGEMENT = "view_memo_excel_management";
    public static final String ACTION_VIEW_MEMO_EXCEL_ATTRIBUTES = "view_memo_excel_attributes";
    public static final String ACTION_UPDATE_MEMO_EXCEL_ATTRIBUTES = "update_memo_excel_attributes";
    public static final String ACTION_VIEW_UPDATE_MEMO_EXCEL_ATTRIBUTES = "view_update_memo_excel_attributes";
    public static final String ACTION_INSERT_MEMO_EXCEL_ATTRIBUTES = "insert_memo_excel_attributes";
    public static final String ACTION_VIEW_INSERT_MEMO_EXCEL_ATTRIBUTES = "view_insert_memo_excel_attributes";
    public static final String ACTION_VIEW_MEMO_PDF_MANAGEMENT = "view_memo_pdf_management";
    public static final String ACTION_VIEW_MEMO_PDF_ATTRIBUTES = "view_memo_pdf_attributes";
    public static final String ACTION_UPDATE_MEMO_PDF_ATTRIBUTES = "update_memo_pdf_attributes";
    public static final String ACTION_VIEW_INSERT_MEMO_PDF_ATTRIBUTES = "view_insert_memo_pdf_attributes";
    public static final String ACTION_INSERT_MEMO_PDF_ATTRIBUTES = "insert_memo_pdf_attributes";
    public static final String ACTION_VIEW_MEMO_PAYMENT_NAMES_TYPES = "view_memo_payment_names_types";
    public static final String ACTION_DELETE_MEMO_PAYMENT_NAMES_TYPES = "delete_memo_payment_names_types";
    public static final String ACTION_VIEW_PAYMENT_ELIGIBILITY_ADDITION = "view_payment_eligibility_addition";
    public static final String ACTION_ADD_PYAMENT_DCM_ELIGIBLE = "add_payment_scm_eligible";
    public static final String ACTION_ADD_PYAMENT_DCM_ELIGIBLE_FROM_GEN = "add_payment_dcm_eligible_from_gen";
    public static final String VECTOR_MEMO_PAYMENTS = "memo_payments";
    public static final String ACTION_VIEW_MEMO_SETTINGS = "view_memo_settings";
    public static final String ACTION_VIEW_EDIT_MEMO_SETTINGS = "view_edit_memo_settings";
    public static final String ACTION_UPDATE_MEMO_SETTINGS = "update_memo_settings";
    public static final String ACTION_VIEW_MEMO_MONITOR = "view_memo_monitor";
    public static final String ACTION_SEARCH_MEMO_MONITOR = "search_memo_monitor";
    public static final String ACTION_EXPORT_MEMO_MONITOR_TO_EXCEL = "export_memo_monitor_to_excel";
    public static final String VECTOR_MEMO_TYPES = "vector_memo_types";
    public static final String VECTOR_MEMO_STATES = "vector_memo_states";
    public static final String VECTOR_DCM_CHANNELS = "vector_dcm_channels";
    public static final String VECTOR_DCM_SUB_CHANNELS = "vector_dcm_sub_channels";
    public static final String VECTOR_DCM_DETAILS_STATUS = "vector_dcm_details_status";
    public static final String VECTOR_DCM_DEDUCTIONS = "vector_dcm_deductions";
    public static final String CONTROL_MEMO_NAME = "memo_name";
    public static final String CONTROL_MEMO_DESCRIPTION = "memo_description";
    public static final String CONTROL_SELECT_PAYMENT_TYPE_ID = "select_payment_type";
    public static final String CONTROL_SELECT_PAYMENT_TYPE_METHOD_ID = "select_payment_type_method";
    public static final String CONTROL_SELECT_SPECIFIC_CHANNEL_ID = "specific_select_channel";
    public static final String CONTROL_SELECT_COLLECTION_CHANNEL_ID = "collection_select_channel";
    public static final String CONTROL_SELECT_SUB_CHANNEL_ID = "select_sub_channel";
    public static final String CONTROL_SELECT_STATE_ID = "select_state";
    public static final String CONTROL_MEMO_ID = "memo_id";
    public static final String CONTROL_HIDDEN_MEMO_ID = "hidden_memo_id";
    public static final String CONTROL_RADIO_SPECIFIC_MEMO = "radio_specific_memo";
    public static final String CONTROL_RADIO_COLLECTION_MEMO = "radio_collection_memo";
    public static final String CONTROL_MEMO_PAYMENT_NAME = "memo_payment_name";
    public static final String CONTROL_MEMO_DCM_NAME = "memo_dcm_name";
    public static final String CONTROL_MEMO_DCM_CODE = "memo_dcm_code";
    public static final String CONTROL_PAYMENT_TYPE = "payment_type";
    public static final String CONTROL_MEMO_DCM_COMMISSION = "memo_dcm_commission";
    public static final String CONTROL_EXISTING_MEMO = "existing_memo";
    public static final String CONTROL_VIEW_MEMO_NAME = "view_memo_name";
    public static final String CONTROL_FILE_NAME = "file_name";
    public static final String CONTROL_ACCRUAL_MESSAGE = "accrual_message";
    public static final String CONTROL_MEMO_REASON_NAME = "memo_reason_name";
    public static final String CONTROL_MEMO_REASON_DESCRIPTION = "memo_reason_description";
    public static final String CONTROL_CHECK_MEMO_CREATION = "check_memo_creation";
    public static final String CONTROL_INSERTED_START_DATE = "inserted_start_date";
    public static final String CONTROL_ADDING_TO_MODULE_START_DATE = "adding_to_module_start_date";
    public static final String CONTROL_SEND_FOR_VALIDATION_DATE = "send_for_validation_date";
    public static final String CONTROL_APPROVAL_DATE = "Approval_date";
    public static final String CONTROL_SALES_MANAGER_APPROVAL_DATE = "Sales_manager_approval_date";
    public static final String CONTROL_SALES_BACK_OFFICE_APPROVAL_DATE = "Sales_back_office_approval_date";
    public static final String CONTROL_SALES_DIRECTIVE_APPROVAL_DATE = "Sales_directivr_approval_date";
    public static final String CONTROL_FINANCE_RECEIVE_DATE = "Finance_receive_date";
    public static final String CONTROL_PAYMENT_DATE = "Payment_date";
    public static final String CONTROL_COMMISSION_CALC_DATE = "commission_calc_date";
    public static final String CONTROL_CALC_TARGETED_DATE = "calc_targeted_date";
    public static final String CONTROL_PAYMENT_TARGETED_DATE = "payment_targeted_date";
    public static final String CONTROL_FINISHED_ON_DATE = "finished_on_date";
    public static final String CONTROL_COMMISSION_CALC_DELAY = "commission_calc_delay";
    public static final String CONTROL_PAYMENT_DELAY = "payment_delay";
    public static final String CONTROL_INSERTED_END_DATE = "inserted_end_date";
    public static final String CONTROL_ADDING_TO_MODULE__END_DATE = "adding_to_module_inserted_end_date";
    public static final String VECTOR_ALL_MEMOS = "all_memos";
    public static final String VECTOR_ALL_QUARTERLY_MEMOS = "all_quarterly_memos";
    public static final String VECTOR_ALL_MONTHLY_MEMOS = "all_monthly_memos";
    public static final String VECTOR_ALL_WEEKLY_MEMOS = "all_weekly_memos";
    public static final String VECTOR_MEMO_MEMBERS = "memo_members";
    public static final String VECTOR_CLOSED_PAYMENTS = "closed_payments";
    public static final String VECTOR_PAYMENT_CONTENTS = "payment_contents";
    public static final String VECTOR_PAYMENT_STAUTS_HISTORY = "payment_status_history";
    public static final String VECTOR_MEMO_HISTORY = "memo_history";
    public static final String VECTOR_MEMO_REASONS = "memo_reasons";
    public static final String VECTOR_MEMO_REPORT_ERROR_MSG = "VECTOR_MEMO_REPORT_ERROR_MSG";
    public static final String VECTOR_MEMO_REPORT_ERROR_MSG_TOBEVIEWED = "VECTOR_MEMO_REPORT_ERROR_MSG_toBeViewed";
    public static final String CONTROL_INCOMING_ACITON = "CONTROL_INCOMING_ACITON";
    public static final String VECTOR_EVERY_MEMO_REASONS = "every_memo_reasons";
    public static final String CONTROL_SELECT_MEMO_REASONS = "control_select_memo_reasons";
    public static final String HIDDEN_MEMO_REASON_ID = "hidden_memo_reason_id";
    public static final String HIDDEN_MEMO_TYPE_ID = "hidden_memo_type_id";
    public static final String HIDDEN_MEMO_ID = "hidden_memo_id";
    public static final String HIDDEN_MEMO_NAME = "hidden_memo_name";
    public static final String HIDDEN_MEMO_COMMENT = "hidden_memo_comment";
    public static final String MEMO_COMMENT = "memo_comment";
    public static final String MEMO_TITLE = "memo_title";
    public static final String HIDDEN_DCM_ID = "hidden_dcm_id";
    public static final String HIDDEN_PAYMENT_ID = "hidden_payment_id";
    public static final String HIDDEN_MEMO_STATE_NAME = "hidden_memo_state_name";
    public static final String HIDDEN_PAYMENT_METHOD = "hidden_payment_method";
    public static final String HIDDEN_MEMO_TYPE_NAME = "hidden_memo_type_name";
    public static final String HIDDEN_OLD_EXCEL_SHEET_NUMBER = "old_hidden_excel_sheet_number";
    //public static final String HIDDEN_PAYMENT_TYPE_NAME="hidden_payment_type_name";
    public static final String HIDDEN_MEMO_TYPE_EXCEL_MANAGEMENT_ID = "hidden_memo_type_excel_management_id";
    public static final String HIDDEN_CHANNEL_ID = "hidden_channel_id";
    public static final String HIDDEN_SUB_CHANNEL_ID = "hidden_sub_channel_id";
    public static final String HIDDEN_FROM_DATE = "hidden_from_date";
    public static final String HIDDEN_TO_DATE = "hidden_to_date";
    public static final String MEMO_TYPE_EXCEL_ATTRIBUTE = "memo_type_excel_attribute";
    public static final String TABLE_DEF_VECTOR = "table_def_vector";
    public static final String CONTROL_DATA_IMPORT_TABLES = "control_table_id";
    public static final String CONTROL_DATA_IMPORT_OPERATION = "control_data_import_operation";
    public static final String QUERY_STRING_TABLES = "TABLE_ID";
    public static final String QUERY_STRING_OPERATION = "operation";
    public static final String DATA_IMPORT_INSERT = "INSERT";
    public static final String DATA_IMPORT_UPDATE = "UPDATE";
    public static final String CONTROL_EXCEL_SHEET_NUMBER = "excel_sheet_number";
    public static final String CONTROL_EXCEL_SHEET_NAME = "excel_sheet_name";
    public static final String CONTROL_EXCEL_SHEET_SQL_STATEMENT = "excel_sheet_sql_statement";
    public static final String CONTROL_NEW_EXCEL_SHEET_NUMBER = "new_excel_sheet_number";
    public static final String CONTROL_NEW_EXCEL_SHEET_NAME = "new_excel_sheet_name";
    public static final String CONTROL_NEW_EXCEL_SHEET_SQL_STATEMENT = "new_excel_sheet_sql_statement";
    public static final String EXCEL_SHEET_MESSAGE = "excel_sheet_message";
    public static final String MEMO_GENERATION_PERIOD = "memo_generation_period";
    public static final String VECTOR_PAYMENT_TYPE_GENERATION_SETTINGS = "vector_payment_type_generation_settings";
    public static final String PAYMENT_TYPE_GENERATION_SETTINGS = "payment_type_generation_settings";
    public static final String VECTOR_GENERATION_PERIODS = "vector_memo_generation_period";
    public static final String HIDDEN_SELECTED_MEMO_GENERATION_PERIOD = "hidden_selected_memo_generation_period";
    public static final String HIDDEN_MEMO_SETTINGS_ID = "hidden_memo_settings_id";
    public static final String HIDDEN_MEMO_PERIOD_ID = "hidden_memo_period_id";
    public static final String HIDDEN_ASSIGNED_PERIOD_ID = "hidden_assigned_period_id";
    public static final String MEMO_PERIOD_ID = "memo_period_id";
    public static final String SELECTED_MEMO_GENERATION_PERIOD = "selected_memo_generation_period";
    public static final String SELECTED_MEMO_TRACKING_FLAG = "selected_memo_tracking_flag";
    public static final String SELECTED_MEMO_PERIOD_WEEK_NO = "selected_memo_period_week_no";
    public static final String CONTROL_INSERTED_QUARTER_DATE = "inserted_quarter_date";
    public static final String MEMO_SETTINGS_ARRAY_DATA = "mem0_settings_array_data";
    public static final String SQL_TEMPLATE_NAME = "template_name";
    public static final String ACTION_ADD_SQL_TEMPLATE = "add_template_action";
    public static final String ACTION_view_ADD_SQL_TEMPLATE = "view_add_template_action";
    public static final String ACTION_VIEW_FINANCE_ISSUED_MEMOS = "view_finance_issued_memos";
    public static final String ACTION_SEARCH_FINANCE_ISSUED_MEMOS = "search_finance_issued_memos";
    public static final String ACTION_FINANCE_ISSUE_ISSUED_MEMO = "finance_issue_issued_memo";
    public static final String ACTION_VIEW_SALES_APPROVAL = "view_sales_approval";
    public static final String ACTION_SEARCH_SALES_APPROVAL = "search_sales_approval";
    public static final String ACTION_SALES_APPROVED = "sales_approved";
    public static final String ACTION_VIEW_SALES_APPROVED_MEMOS = "view_sales_approved_memos";
    public static final String ACTION_SEARCH_SALES_APPROVED_MEMOS = "search_sales_approved_memos";
    public static final String ACTION_SALES_APPROVED_TO_AP = "sales_approved_to_ap";
    public static final String ACTION_SALES_APPROVED_TO_LOGESTIC = "sales_approved_to_logestic";
    public static final String ACTION_SALES_APPROVED_TO_CREDIT_MODULE = "sales_approved_to_credit_module";
    public static final String ACTION_VIEW_SALES_APPROVED_TO_AP = "view_sales_approved_to_ap";
    public static final String ACTION_SEARCH_SALES_APPROVED_TO_AP = "search_sales_approved_to_ap";
    public static final String ACTION_VIEW_SALES_APPROVED_TO_LOGESTIC = "view_sales_approved_to_logestic";
    public static final String ACTION_SEARCH_SALES_APPROVED_TO_LOGESTIC = "search_sales_approved_to_logestic";

}