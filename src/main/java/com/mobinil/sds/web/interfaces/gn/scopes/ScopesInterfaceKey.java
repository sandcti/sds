package com.mobinil.sds.web.interfaces.gn.scopes;

public interface ScopesInterfaceKey  {

public static final String CACH_OBJ_SCOPES_STATUS   = "cach_obj_scopes_status";

///////////////List of action keys//////////////////////
public static final String SCOPE_ACTION   = "scope_action";
public static final String HASHMAP_KEY_SCOPE_COLLECTION = "hashmap_key_scope_collection";
public static final String HASHMAP_KEY_SCOPE_FIELD_COLLECTION = "hashmap_key_scope_field_collection";

///////////////List of actions for scopes///////////////
public static final String ACTION_SHOW_ALL_SCOPES   = "show_all_scopes";
public static final String ACTION_SAVE_SCOPE_STATUS = "save_scope_status";
public static final String ACTION_ADD_SCOPE = "add_scope";
public static final String ACTION_EDIT_SCOPE = "edit_scope";
public static final String ACTION_SAVE_SCOPE = "save_scope";

public static final String ACTION_SHOW_ALL_DATA_VIEWS   = "show_all_data_views";
public static final String ACTION_SAVE_DATA_VIEW_STATUS   = "save_data_view_status";
///////////////List of controls///////////////////////// 
public static final String CONTROL_HIDDEN_SCOPE_ID = "hidden_scope_id" ;
public static final String CONTROL_TEXTAREA_SCOPE_NAME = "textarea_scope_name";
public static final String CONTROL_SELECT_SCOPE_STATUS = "select_scope_status";
public static final String CONTROL_TEXT_SCOPE_ISSUE = "text_scope_issue";
public static final String CONTROL_TEXT_SCOPE_VERSION = "text_scope_version";
public static final String CONTROL_TEXTAREA_SCOPE_DESCRIPTION = "textarea_scope_description";

public static final String CONTROL_SELECT_DATA_VIEW_STATUS =  "dataviewstatus";
public static final String CONTROL_HIDDEN_DATA_VIEW_PREVIOUS_STATUS =  "data_view_previous_status";
public static final String CONTROL_HIDDEN_DATA_VIEW_NAME =  "data_view_name";
}