package com.mobinil.sds.web.interfaces.ma;

public interface MAInterfaceKey 
{
	
	/****************************** Maintenance Actions************************/
	public static final String ACTION_SHOW_ALL_MODULES="show_all_modules";
	public static final String ACTION_EDIT_MODULE="edit_module";
	public static final String ACTION_CREATE_NEW_MODULE="create_new_module";
	public static final String ACTION_DELETE_MODULE="delete_module";
	public static final String ACTION_UPDATE_MODULE="update_module";
	public static final String ACTION_SAVE_MODULE="save_module";
	public static final String ACTION_SHOW_ALL_PRIVILAGES="show_all_privilages";
	public static final String ACTION_DELETE_PRIVILAGE="delete_privilage";
	public static final String ACTION_CREATE_PRIVILAGE="create_privilage";
	public static final String ACTION_UPDATE_PRIVILAGE="update_privilage";
	public static final String ACTION_SAVE_PRIVILAGE="save_privilage";
	public static final String ACTION_EDIT_PRIVILAGE="edit_privilage";
	/******************************Vectors**************************************/

	public static final String  VECTOR_SCHEMA_MODULE="vector_schema_module";
	public static final String  VECTOR_SCHEMA_MODULE_2="vector_schema_module_2";  
	public static final String  VECTOR_SCHEMA_MODULE_3="vector_schema_module_3";
	/****************************** Maintenance Controls************************/

	public static final String INPUT_HIDDEN_MODULE_ID="hidden_module_id";
	public static final String INPUT_SELECT_MODULE_ID="select_module_id";
	public static final String INPUT_TEXT_MODULE_NAME="text_module_name";
	public static final String INPUT_TEXT_MODULE_DESC="text_module_desc";
	public static final String INPUT_HIDDEN_PRIVILAGE_ID="hidden_privilage_id";
	public static final String INPUT_TEXT_PRIVILAGE_NAME="privilage_name";
	public static final String INPUT_TEXT_PRIVILAGE_DESC="privilage_desc";
	public static final String INPUT_TEXT_PRIVILAGE_STATUS_NAME="privilage_status_name";
	public static final String INPUT_TEXT_PRIVILAGE_ACTION_NAME="privilage_action_name";
	public static final String INPUT_TEXT_ORDER_VALUE="order_value";
	public static final String INPUT_TEXT_PRIVILAGE_TARGET="privilage_target";
	public static final String INPUT_SELECT_MODULE_NAME="select_module_name";
	public static final String INPUT_SELECT_PRIVILAGE_STATUS_NAME="select_privilage_status_name";
	public static final String INPUT_SEARCH_MODULE_NAME="SEARCH_MODULE_NAME";
}
