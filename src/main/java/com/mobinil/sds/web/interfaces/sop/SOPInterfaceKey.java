package com.mobinil.sds.web.interfaces.sop;

/**
 * SOPInterfaceKey interface holding all the keys used in the SOP Module.
 *
 * @version	1.12 Apr 2007
 * @author Waseem Safwat Adly
 * @see
 *
 * SDS MobiNil
 */
public interface SOPInterfaceKey {
    ////////////////////////////////////////////////////////////////////////
  /*
     * SOP Actions Keys /
     *//////////////////////////////////////////////////////////////////////

    public static final String ACTION_SHOW_ALL_SCHEMAS = "show_all_schemas";
    public static final String ACTION_SHOW_ALL_SCHEMAS_FOR_FRANCHISE = "show_all_schemas_for_franchise";
    public static final String ACTION_SHOW_ALL_SCHEMAS_FOR_COMPLEMENTARY_CHANNEL = "show_all_schemas_for_complementary_channel";
    public static final String ACTION_CREATE_NEW_REQUEST_FOR_COMPLEMANTRY = "create_new_request_for_complemantry";
    public static final String ACTION_CREATE_NEW_SCHEMA = "create_new_schema";
    public static final String ACTION_VIEW_SCHEMA_PRODUCTS = "view_schema_products";
    public static final String ACTION_SAVE_SCHEMA = "save_schema";
    public static final String ACTION_UPDATE_SCHEMA_PRODUCTS = "update_schema_products";
    public static final String ACTION_DELETE_SCHEMA_PRODUCT = "delete_schema_product";
    public static final String ACTION_IMPORT_PRODUCTS = "import_products";
    public static final String ACTION_IMPORT_PRODUCTS_BY_CHANNEL = "import_products_by_channel";
    public static final String ACTION_IMPORT_PRODUCTS_FOR_FRANCHISE = "import_products_for_franchise";
    public static final String ACTION_IMPORT_PRODUCTS_FOR_COMPLEMENTARY = "import_products_for_complementary";
    public static final String ACTION_IMPORT_PRODUCTS_AND_CREATE_SCHEMA = "import_products_and_create_schema";
    public static final String ACTION_IMPORT_PRODUCTS_FROM_PGW = "import_products_from_pgw";
    public static final String ACTION_CHANGE_SCHEMA_STATUS = "change_schema_status";
    public static final String ACTION_SEARCH_SCHEMA = "search_schema";
    public static final String ACTION_CREATE_NEW_REQUEST = "create_new_request";
    public static final String ACTION_CREATE_NEW_REQUEST_FOR_FRANCHISE = "create_new_request_for_franchise";
    public static final String ACTION_ADMIN_CREATE_NEW_REQUEST = "admin_create_new_request";
    public static final String ACTION_ADMIN_CREATE_NEW_REQUEST_FOR_FRANCHISE = "admin_create_new_request_for_franchise";
    public static final String ACTION_ADMIN_CREATE_NEW_REQUEST_FOR_COMPLEMANTRY = "admin_create_new_request_for_complemantry";
    public static final String ACTION_REQUEST_SCHEMA_PRODUCTS = "request_schema_product";
    public static final String ACTION_ADMIN_REQUEST_SCHEMA_PRODUCTS = "admin_request_schema_product";
    public static final String ACTION_SAVE_REQUEST = "save_request";
    public static final String ACTION_ADMIN_SAVE_REQUEST = "admin_save_request";
    public static final String ACTION_SHOW_ALL_REQUESTS = "show_all_requests";
    public static final String ACTION_SHOW_ALL_REQUESTS_BY_CHANNEL = "show_all_requests_by_channel";
    public static final String ACTION_SHOW_ALL_REQUESTS_FOR_FRANCHISE = "show_all_requests_for_franchise";
    public static final String ACTION_SHOW_ALL_REQUESTS_FOR_COMPLEMANTRY = "show_all_requests_for_complemantry";
    public static final String ACTION_UPDATE_REQUEST_STATUS = "update_request_status";
    public static final String ACTION_VIEW_REQUEST_DETAILS = "view_request_details";
    public static final String ACTION_PRINT_REQUEST_PAYMENT_SLIP = "print_request_payment_slip";
    public static final String ACTION_DATA_WAREHOUSE_IMPORT = "data_warehouse_import";
    public static final String ACTION_DATA_WAREHOUSE_IMPORT_FOR_FRANCHISE = "data_warehouse_import_for_franchise";
    public static final String ACTION_DATA_WAREHOUSE_IMPORT_FOR_COMPLEMANTRY = "data_warehouse_import_for_complemantry";
    public static final String ACTION_DATA_WAREHOUSE_IMPORT_PROCESS = "data_warehouse_import_process";
    public static final String ACTION_GENERATE_DATA_WAREHOUSE_IMPORT_TEMPLATE = "generate_data_warehouse_import_template";
    public static final String ACTION_VIEW_STOCK_PRODUCTS = "view_stock_products";
    public static final String ACTION_VIEW_STOCK_PRODUCTS_BY_CHANNEL_WAREHOUSE = "view_stock_products_by_channel_warehouse";
    public static final String ACTION_VIEW_STOCK_PRODUCTS_FOR_FRANCHISE = "view_stock_products_for_franchise";
    public static final String ACTION_VIEW_STOCK_PRODUCTS_FOR_COMPLEMANTRY = "view_stock_products_for_complemantry";
    public static final String ACTION_SEARCH_STOCK_PRODUCT = "search_stock_product";
    public static final String ACTION_GENERATE_MANUAL_DCM_PRODUCT_LIMITS = "generate_manual_dcm_product_limits";
    public static final String ACTION_GENERATE_MANUAL_DCM_PRODUCT_LIMITS_CHECKING = "generate_manual_dcm_product_limits_checking";
    public static final String ACTION_ADMIN_SELECT_DCM_REQUEST_PRODUCT_LIMITS = "admin_select_dcm_request_product_limits";
    public static final String ACTION_ADMIN_SET_DCM_REQUEST_PRODUCT_LIMITS = "admin_set_dcm_request_product_limits";
    public static final String ACTION_SAVE_DCM_REQUEST_PRODUCT_LIMITS = "save_dcm_request_product_limits";
    public static final String ACTION_SHOW_ALL_EQUATIONS = "show_all_equations";
    public static final String ACTION_EDIT_EQUATION = "edit_equation";
    public static final String ACTION_ASSIGN_EQUATION_TO_PRODUCTS = "assign_equation_to_products";
    public static final String ACTION_SAVE_ASSIGN_EQUATION_TO_PRODUCTS = "save_assign_equation_to_products";
    public static final String ACTION_DCM_PRODUCT_LIMITS_BY_EQUATION = "dcm_product_limits_by_equation";
    public static final String ACTION_SAVE_DCM_PRODUCT_LIMITS_BY_EQUATION = "save_dcm_product_limits_by_equation";
    public static final String ACTION_DCM_PRODUCT_LIMITS_BY_EQUATION_SELECT_PRODUCTS = "dcm_product_limits_by_equation_select_products";
    public static final String ACTION_ADD_EQUATION = "add_equation";
    public static final String ACTION_UPDATE_EQUATION = "update_equation";
    public static final String ACTION_UPDATE_EQUATION_STATUS = "update_equation_status";
    public static final String ACTION_SAVE_EQUATION = "save_equation";
    public static final String ACTION_SELECT_DCM_QUOTA_SETTINGS = "select_dcm_quota_settings";
    public static final String ACTION_SELECT_DCM_QUOTA_SETTINGS_BY_CHANNEL_SCM = "select_dcm_quota_settings_by_channel_scm";
    public static final String ACTION_SELECT_FRANCHISE_QUOTA_SETTINGS = "select_franchise_quota_settings";
    public static final String ACTION_SELECT_COMPLEMANTRY_QUOTA_SETTINGS = "select_complemantry_quota_settings";
    public static final String ACTION_SET_DCM_QUOTA_SETTING_VALUES = "set_dcm_quota_setting_values";
    public static final String ACTION_UPDATE_DCM_QUOTA_SETTING_VALUE = "update_dcm_quota_setting_value";
    public static final String ACTION_SELECT_DCM_QUOTA_VALUES = "select_dcm_quota_values";
    public static final String ACTION_SELECT_FRANCHISE_QUOTA_VALUES = "select_franchise_quota_values";
    public static final String ACTION_SELECT_COMPLEMANTRY_QUOTA_VALUES = "select_complemantry_quota_values";
    public static final String ACTION_SET_DCM_QUOTA_VALUES = "set_dcm_quota_values";
    public static final String ACTION_UPDATE_DCM_QUOTA_VALUE = "update_dcm_quota_value";
    public static final String ACTION_SHOW_USER_DCM_SCREEN_SOP = "show_user_dcm_screen_sop";
    public static final String ACTION_SHOW_USER_DCM_SCREEN_SOP_BY_CHANNEL = "show_user_dcm_screen_sop_by_channel";
    public static final String ACTION_SHOW_USER_FRANCHISE_SCREEN_SOP = "show_user_franchise_screen_sop";
    public static final String ACTION_SHOW_USER_COMPLEMANTRY_SCREEN_SOP = "show_user_complemantry_screen_sop";
    public static final String ACTION_SOP_UPDATE_USER_DCM = "sop_update_user_dcm";
    public static final String ACTION_SEARCH_REQUEST = "search_request";
    public static final String ACTION_FIXED_QUOTA_VALUES = "fixed_quota_values";
    public static final String ACTION_FIXED_QUOTA_VALUES_BY_CHANNEL = "fixed_quota_values_by_channel";
    public static final String ACTION_FIXED_QUOTA_VALUES_FOR_FRANCHISE = "fixed_quota_values_for_franchise";
    public static final String ACTION_FIXED_QUOTA_VALUES_FOR_COMPLEMANTRY = "fixed_quota_values_for_complemantry";
    public static final String ACTION_SAVE_FIXED_QUOTA_VALUES = "save_fixed_quota_values";
    public static final String ACTION_REQUEST_NOTIFICATION_MANAGEMENT = "request_notification_management";
    public static final String ACTION_REQUEST_NOTIFICATION_MANAGEMENT_BY_CHANNEL = "request_notification_management_by_channel";
    public static final String ACTION_REQUEST_NOTIFICATION_MANAGEMENT_FOR_FRANCHISE = "request_notification_management_for_franchise";
    public static final String ACTION_REQUEST_NOTIFICATION_MANAGEMENT_FOR_COMPLEMENTARY = "request_notification_management_for_complementary";
    public static final String ACTION_REQUEST_NOTIFICATION_MANAGEMENT_FOR_AUTHORIZED_AGENT = "request_notification_management_for_authorized_agent";
    public static final String ACTION_EDIT_REQUEST_NOTIFICATION = "edit_request_notification";
    public static final String ACTION_DELETE_REQUEST_NOTIFICATION = "delete_request_notification";
    public static final String ACTION_CREATE_REQUEST_NOTIFICATION = "create_request_notification";
    public static final String ACTION_SAVE_REQUEST_NOTIFICATION = "save_request_notification";
    public static final String ACTION_QUOTA_CALCULATION_BY_DATAVIEW = "quota_calculation_by_dataview";
    public static final String ACTION_SAVE_QUOTA_CALCULATION_BY_DATAVIEW = "save_quota_calculation_by_dataview";
    public static final String ACTION_IMPORT_LCS_PRODUCTS_PHYSICAL_AMOUNT = "import_lcs_products_physical_amount";
    public static final String ACTION_IMPORT_LCS_PRODUCTS_PYHSICAL_AMOUNT_BY_CHANNEL_WAREHOUSE = "import_lcs_products_physical_amount_by_channel_warehouse";
    public static final String ACTION_DELETE_IMPORTED_PRODUCTS_FOR_DISTRIBUTORS = "delete_imported_products_for_distributors";
    public static final String ACTION_DELETE_IMPORTED_PRODUCTS_FOR_DISTRIBUTORS_BY_CHANNEL = "delete_imported_products_for_distributors_by_channel";
    public static final String ACTION_DELETE_IMPORTED_PRODUCTS_FOR_FRANCHISE = "delete_imported_products_for_franchise";
    public static final String ACTION_DELETE_IMPORTED_PRODUCTS_FOR_COMPLEMANTRY = "delete_imported_products_for_complemantry";
    public static final String ACTION_IMPORT_LCS_PRODUCTS_PHYSICAL_AMOUNT_FOR_FRANCHISE = "import_lcs_products_physical_amount_for_franchise";
    public static final String ACTION_IMPORT_LCS_PRODUCTS_PHYSICAL_AMOUNT_FOR_COMPLEMANTRY = "import_lcs_products_physical_amount_for_complemantry";
    public static final String ACTION_CALCULATE_QUOTA_BY_DATAVIEW = "calculate_quota_by_dataview";
    public static final String ACTION_CREATE_NEW_PRODUCT_PGW = "create_new_product_pgw";
    public static final String ACTION_SAVE_PRODUCT_PGW = "save_product_pgw";
    public static final String ACTION_EDIT_PRODUCT_PGW = "edit_product_pgw";
    public static final String ACTION_DELETE_PRODUCT_FROM_PGW = "delete_product_from_pgw";
    public static final String ACTION_UPDATE_PRODUCT_PGW = "update_product_pgw";
    public static final String ACTION_CREATE_NEW_INVOICE = "create_new_invoice";
    public static final String ACTION_SAVE_NEW_INVOICE = "save_new_invoice";
    public static final String ACTIOM_EDIT_INVOICE = "edit_invoice";
    public static final String ACTION_UPDATE_INVOICE_DATA = "update_invoice_data";
    public static final String ACTION_ADD_EDIT_INVOICE_DETAIL = "add_edit_invoice_detail";
    public static final String ACTION_SHOW_INVOICE_DETAIL_DATA = "show_invoice_detail_data";
    public static final String ACTION_SAVE_INVOICE_DETAIL = "save_invoice_detail";
    public static final String ACTION_CREATE_NEW_INVOICE_DETAIL = "create_new_invoice_detail";
    public static final String ACTION_UPDATE_INVOICE_DETAIL_DATA = "update_invoice_detail_data";
    public static final String ACTION_SHOW_PRODUCTS_FROM_LCS = "show_products_from_lcs";
    public static final String ACTION_CREATE_PRODUCT_LCS = "create_product_lcs";
    public static final String ACTION_SAVE_PRODUCT_LCS = "save_product_lcs";
    public static final String ACTION_EDIT_PRODUCT_LCS = "edit_product_lcs";
    public static final String ACTION_DELETE_PRODUCT_LCS = "delete_product_lcs";
    public static final String ACTION_UPDATE_PRODUCT_LCS = "update_product_lcs";
    public static final String ACTION_SHOW_ALL_INVOICES = "show_all_invoices";
    public static final String ACTION_SEARCH_INVOICE = "search_invoice";
    public static final String ACTION_INVOICE_BACK = "invoice_back";
    public static final String ACTION_SHOW_ALL_PRODUCTS_PGW = "show_all_products_pgw";
    public static final String ACTION_SHOW_ALL_PRODUCTS_PGW_BY_CHANNEL = "show_all_products_pgw_by_channel";
    public static final String ACTION_SHOW_ALL_PRODUCTS_PGW_FOR_FRANCHISE = "show_all_products_pgw_for_franchise";
    public static final String ACTION_SHOW_ALL_PRODUCTS_PGW_FOR_COMPLEMANTRY = "show_all_products_pgw_for_complemantry";
    public static final String ACTION_SEARCH_PRODUCT_PGW = "search_product_pgw";
    public static final String ACTION_SHOW_SCRATCH_REPORT_FOR_FRANCHISE = "show_scratch_report_for_franchise";
    public static final String ACTION_SHOW_SCRATCH_REPORT_FOR_COMPLEMENTARY = "show_scratch_report_for_complementary";
    public static final String ACTION_SHOW_SCRATCH_REPORT = "show_scratch_report";
    public static final String ACTION_SHOW_SCRATCH_REPORT_BY_CHANNEL = "show_scratch_report_by_channel";
    public static final String ACTION_SEARCH_SCRATCH_REPORT = "search_scratch_report";
    public static final String ACTION_EXPORT_SCRATCH_REPORT = "export_scratch_report";
    public static final String ACTION_SHOW_PRODUCT_REPORT_FOR_FRANCHISE = "show_product_report_for_franchise";
    public static final String ACTION_SHOW_PRODUCT_REPORT_FOR_COMPLEMENTARY = "show_product_report_for_complementary";
    public static final String ACTION_SHOW_PRODUCT_REPORT = "show_product_report";
    public static final String ACTION_SHOW_PRODUCT_REPORT_BY_CHANNEL = "show_product_report_by_channel";
    public static final String ACTION_SEARCH_PRODUCT_REPORT = "search_product_report";
    public static final String ACTION_EXPORT_PRODUCT_REPORT = "export_product_report";
    public static final String ACTION_SHOW_TOTAL_REPORT_FOR_FRANCHISE = "show_total_report_for_franchise";
    public static final String ACTION_SHOW_TOTAL_REPORT_FOR_COMPLEMENTARY = "show_total_report_for_complementary";
    public static final String ACTION_SHOW_TOTAL_REPORT = "show_total_report";
    public static final String ACTION_SHOW_TOTAL_REPORT_BY_CHANNEL = "show_total_report_by_channel";
    public static final String ACTION_SEARCH_TOTAL_REPORT = "search_total_report";
    public static final String ACTION_EXPORT_TOTAL_REPORT = "export_total_report";
    public static final String DCM_QUOTA_ACTION = "dcm_quota_action";
    public static final String ACTION_SHOW_ALL_REQUESTS_CHANGE_TO_PAID = "show_all_requests_change_to_paid";
    public static final String ACTION_SHOW_ALL_REQUESTS_CHANGE_TO_PAID_BY_CHANNEL = "show_all_requests_change_to_paid_by_channel";
    public static final String ACTION_SHOW_ALL_REQUESTS_CHANGE_TO_PAID_FOR_FRANCHISE = "show_all_requests_change_to_paid_for_franchise";
    public static final String ACTION_SHOW_ALL_REQUESTS_CHANGE_TO_PAID_FOR_COMPLEMANTRY = "show_all_requests_change_to_paid_for_complemantry";
    public static final String Action_SHOW_ALL_SCHEMAS_BY_CHANNEL = "show_all_schemas_by_channel";
    public static final String ACTION_SEARCH_REQUEST_CHANGE_TO_PAID = "search_request_change_to_paid";
    public static final String ACTION_UPDATE_REQUEST_STATUS_CHANGE_TO_PAID = "update_request_status_change_to_paid";
    public static final String ACTION_GO_TO_SHOW_FRANCHISE_STATE = "go_to_show_franchise_state";
    public static final String ACTION_SEARCH_FRANCHISE_STATE = "search_franchise_state";
    public static final String ACTION_NEXT_FRANCHISE_STATE = "next_franchise_state";
    public static final String ACTION_PREVIOUS_FRANCHISE_STATE = "previous_franchise_state";
    public static final String ACTION_EXCEL_FRANCHISE_STATE = "excel_franchise_state";
    public static final String ACTION_SEARCH_LCS_STATE = "search_lcs_state";
    public static final String ACTION_NEXT_LCS_STATE = "next_lcs_state";
    public static final String ACTION_PREVIOUS_LCS_STATE = "previous_lcs_state";
    public static final String ACTION_EXCEL_LCS_STATE = "excel_lcs_state";
    public static final String ACTION_SEARCH_PGW_STATE = "search_pgw_state";
    public static final String ACTION_NEXT_PGW_STATE = "next_pgw_state";
    public static final String ACTION_PREVIOUS_PGW_STATE = "previous_pgw_state";
    public static final String ACTION_EXCEL_PGW_STATE = "excel_pgw_state";
    public static final String ACTION_GO_TO_SHOW_FRANCHISE_DETAILS = "go_to_show_franchise_details";
    public static final String ACTION_SEARCH_FRANCHISE_DETAILS = "search_franchise_details";
    public static final String ACTION_NEXT_FRANCHISE_DETAILS = "next_franchise_details";
    public static final String ACTION_PREVIOUS_FRANCHISE_DETAILS = "previous_franchise_details";
    public static final String ACTION_VIEW_PRODUCT_REPORT = "view_product_report";
    public static final String ACTION_SUBMIT_PRODUCTS = "submit_products";
    public static final String ACTION_SEARCH_PRODUCTS = "search_products";
    public static final String ACTION_IMPORT_PRODUCTS_FOR_AUTHORIZED_AGENT = "import_products_for_authorized_agent";
    public static final String ACTION_SHOW_ALL_SCHEMAS_FOR_AUTHORIZED_AGENT = "show_all_schemas_for_authorized_agent";
    public static final String ACTION_CREATE_NEW_REQUEST_FOR_AUTHORZED_AGENT = "create_new_request_for_authorized_agent";
    public static final String ACTION_ADMIN_CREATE_NEW_REQUEST_FOR_AUTHORIZED_AGENT = "admin_create_new_request_for_authorized_agent";
    public static final String ACTION_SHOW_ALL_REQUESTS_CHANGE_TO_PAID_FOR_AUTHORIZED_AGENT = "show_all_requests_change_to_paid_for_authorized_agent";
    public static final String ACTION_SHOW_ALL_REQUESTS_FOR_AUTHORIZED_AGENT = "show_all_requests_for_authorized_agent";
    public static final String ACTION_SELECT_AUTHORIZED_AGENT_QUOTA_SETTINGS = "select_authorized_agent_quota_settings";
    public static final String ACTION_SELECT_AUTHORIZED_AGENT_QUOTA_VALUES = "select_authorized_agent_quota_values";
    public static final String ACTION_FIXED_QUOTA_VALUES_FOR_AUTHORIZED_AGENT = "fixed_quota_values_for_authorized_agent";
    public static final String ACTION_SHOW_USER_AUTHORIZED_AGENT_SCREEN_SOP = "show_user_authorized_agent_screen_sop";
    public static final String ACTION_DATA_WAREHOUSE_IMPORT_FOR_AUTHORIZED_AGENT = "data_warehouse_import_for_authorized_agent";
    public static final String ACTION_VIEW_STOCK_PRODUCTS_FOR_AUTHORIZED_AGENT = "view_stock_products_for_authorized_agent";
    public static final String ACTION_IMPORT_LCS_PRODUCTS_PHYSICAL_AMOUNT_FOR_AUTHORIZED_AGENT = "import_lcs_products_physical_amount_for_authorized_agent";
    public static final String ACTION_SHOW_ALL_PRODUCTS_PGW_FOR_AUTHORIZED_AGENT = "show_all_products_pgw_for_authorized_agent";
    public static final String ACTION_DELETE_IMPORTED_PRODUCTS_FOR_AUTHORIZED_AGENT = "delete_imported_products_for_authorized_agent";
    public static final String ACTION_SHOW_SCRATCH_REPORT_FOR_AUTHORIZED_AGENT = "show_scratch_report_for_authorized_agent";
    public static final String ACTION_SHOW_PRODUCT_REPORT_FOR_AUTHORIZED_AGENT = "show_product_report_for_authorized_agent";
    public static final String ACTION_SHOW_TOTAL_REPORT_FOR_AUTHORIZED_AGENT = "show_total_report_for_authorized_agent";
    public static final String ACTION_VIEW_WAREHOUSE_CHANNEL = "view_warehouse_channel";
    public static final String ACTION_EDIT_WAREHOUSE_CHANNEL = "edit_warehouse_channel";
    public static final String ACTION_ADD_WAREHOUSE_CHANNEL = "add_warehouse_channel";
    public static final String ACTION_UPDATE_WAREHOUSE_CHANNEL = "update_warehouse_channel";
    public static final String ACTION_SAVE_WAREHOUSE_CHANNEL = "save_warehouse_channel";
    public static final String ACTION_DELETE_WAREHOUSE_CHANNEL = "delete_warehouse_channel";
    public static final String ACTION_USER_CHANNEL_LIST = "user_channel_list";
    public static final String ACTION_UPDATE_USER_CHANNEL_LIST = "update_user_channel_list";
    public static final String ACTION_REQUEST_ADMIN_CHANGE_STATUS = "request_admin_change_status";
    public static final String ACTION_VIEW_REQUEST_STATUS_LIST = "view_request_status_list";
    public static final String ACTION_SEARCH_REQUEST_STATUS_LIST = "search_request_status_list";
    public static final String ACTION_UPDATE_REQUEST_STATUS_LIST = "update_request_status_list";
    public static final String ACTION_VIEW_WAREHOUSE_LIST = "view_warehouse_list";
    public static final String ACTION_DELETE_WAREHOUSE_LIST = "delete_warehouse_list";
    public static final String ACTION_CREATE_NEW_WAREHOUSE = "create_new_warehouse";
    public static final String ACTION_SAVE_NEW_WAREHOUSE = "save_new_warehouse";
    public static final String ACTION_SHOW_USER_DELETE_CONTROL = "show_user_delete_control";
    public static final String ACTION_USER_DELETE_CONTROL = "user_delete_control";
    ////////////////////////////////////////////////////////////////////////
  /*
     * SOP Controls /
     *//////////////////////////////////////////////////////////////////////
    public static final String INPUT_HIDDEN_ROW_NUM = "hidden_row_num";
    public static final String INPUT_HIDDEN_COUNT = "hidden_count";
    public static final String INPUT_HIDDEN_SCHEMA_ID = "hidden_schema_id";
    public static final String INPUT_HIDDEN_SCHEMA_CODE = "hidden_schema_code";
    public static final String INPUT_HIDDEN_SCHEMA_NAME = "hidden_schema_name";
    public static final String INPUT_HIDDEN_INVOICE_NUMBER = "hidden_invoice_number";
    public static final String INPUT_HIDDEN_PAYMENT_DATE = "hidden_payment_date";
    public static final String INPUT_HIDDEN_LCS_PRODCUT_CODE = "hidden_lcs_product_code";
    public static final String INPUT_HIDDEN_PRODUCT_NAME_IN_ENGLISH = "hidden_product_name_in_english";
    public static final String INPUT_HIDDEN_PRODUCT_NAME_IN_ARABIC = "hidden_product_name_in_arabic";
    public static final String INPUT_TEXT_INVOICE_NUMBER = "invoice_number";
    public static final String INPUT_TEXT_WAREHOUSE_NAME = "text_warehouse_name";
    public static final String INPUT_HIDDEN_INVOICE_DETAIL_ID = "invoice_detail_id";
    public static final String INPUT_TEXT_DCM_ID = "dcm_id";
    public static final String INPUT_TEXT_TOTAL_AMOUNT = "total_amount";
    public static final String INPUT_TEXT_PAYMENT_SERIAL_NUMBER = "payment_serial_number";
    public static final String INPUT_TEXT_PAYMENT_DATE = "payment_date";
    public static final String INPUT_TEXT_SCHEMA_PRODUCT_ID = "schema_product_id";
    public static final String INPUT_TEXT_PRODUCT_QUANTITY = "product_quantity";
    //public static final String INPUT_TEXT_PRODUCT_PRICE = "product_price";
    public static final String INPUT_TEXT_SCHEMA_CODE = "text_schema_code";
    public static final String INPUT_TEXT_PRODUCT_ID = "Product_ID";
    public static final String INPUT_TEXT_LCS_PRODCUT_CODE = "LCS_Product_Code";
    public static final String INPUT_TEXT_HAS_QUANTITY = "Has_Quantity";
    public static final String INPUT_TEXT_IS_ACTIVE = "IS_Active";
    public static final String INPUT_TEXT_PRODUCT_NAME_IN_ENGLISH = "Product_name_in_English";
    public static final String INPUT_TEXT_PRODUCT_NAME_IN_ARABIC = "Product_name_in_Arabic";
    public static final String INPUT_TEXT_PRODUCT_PRICE = "Product_Price";
    public static final String INPUT_TEXT_SALES_TAX = "Sales_Tax";
    public static final String INPUT_TEXT_SCHEMA_NAME = "text_schema_name";
    public static final String INPUT_TEXT_SCHEMA_DESCRIPTION = "text_schema_description";
    public static final String INPUT_HIDDEN_PRODUCT_ID = "hidden_product_id";
    public static final String INPUT_HIDDEN_SCHEMA_NEXT_STATUS_ID = "hidden_schema_next_status_id";
    public static final String INPUT_TEXT_PRODUCT_WEIGHT = "text_product_weight";
    public static final String INPUT_TEXT_PRODUCT_DISCOUNT = "text_product_discount";
    public static final String INPUT_HIDDEN_PRODUCT_DISCOUNT = "hidden_product_discount";
    public static final String INPUT_HIDDEN_PRODUCT_DISCOUNT_AMOUNT = "hidden_product_discount_amount";
    public static final String INPUT_HIDDEN_PRODUCT_NET_AMOUNT = "hidden_product_net_amount";
    public static final String INPUT_HIDDEM_PRODUCT_PRICE = "hidden_product_Price";
    public static final String INPUT_SELECT_PRODUCT_IS_POINT = "select_product_is_point";
    public static final String INPUT_SELECT_PRODUCT_IS_QUOTA = "select_product_is_quota";
    public static final String INPUT_SELECT_SCHEMA_ID = "select_schema_id";
    public static final String INPUT_TEXT_ITEM_NAME = "text_item_name";
    public static final String INPUT_TEXT_PGW_ITEM_CODE = "text_pgw_item_code";
    public static final String INPUT_TEXT_LCS_ITEM_CODE = "text_lcs_item_code";
    public static final String INPUT_TEXT_LCS_ITEM_DESCRIPTION = "text_lcs_item_description";
    public static final String INPUT_SEARCH_TEXT_SCHEMA_CODE = "search_text_schema_code";
    public static final String INPUT_SEARCH_TEXT_SCHEMA_NAME = "search_text_schema_name";
    public static final String INPUT_SEARCH_TEXT_CREATION_DATE_FROM = "search_text_creation_date_from";
    public static final String INPUT_SEARCH_TEXT_CREATION_DATE_TO = "search_text_creation_date_to";
    public static final String INPUT_SEARCH_TEXT_PAYMENT_DATE_FROM = "search_text_payment_date_from";
    public static final String INPUT_SEARCH_TEXT_PAYMENT_DATE_TO = "search_text_payment_date_to";
    public static final String INPUT_SEARCH_TEXT_START_DATE_FROM = "search_text_start_date_from";
    public static final String INPUT_SEARCH_TEXT_START_DATE_TO = "search_text_start_date_to";
    public static final String INPUT_SEARCH_TEXT_END_DATE_FROM = "search_text_end_date_from";
    public static final String INPUT_SEARCH_TEXT_END_DATE_TO = "search_text_end_date_to";
    public static final String INPUT_SEARCH_TEXT_PAYMENT_DATE_IN = "search_text_payment_date_in";
    public static final String INPUT_SEARCH_SELECT_SCHEMA_STATUS = "search_select_schema_status";
    public static final String INPUT_SEARCH_SELECT_CHANNEL_ID = "search_select_channel_id";
    public static final String INPUT_HIDDEN_REQUEST_CODE = "hidden_request_code";
    public static final String INPUT_SEARCH_TEXT_REQUEST_CODE = "search_text_request_code";
    public static final String INPUT_SEARCH_TEXT_REQUEST_ID = "search_text_request_id";
    public static final String INPUT_SEARCH_SELECT_DCM_NAME = "search_select_dcm_name";
    public static final String INPUT_SEARCH_TEXT_REQUEST_CREATION_DATE_FROM = "search_text_request_creation_date_from";
    public static final String INPUT_SEARCH_TEXT_REQUEST_CREATION_DATE_TO = "search_text_request_creation_date_to";
    public static final String INPUT_SEARCH_SELECT_REQUEST_STATUS = "search_select_request_status";
    public static final String INPUT_SEARCH_TEXT_INVOICE_NUMBER = "search_text_invoice_number";
    public static final String INPUT_SEARCH_TEXT_DCM_ID = "search_text_dcm_id";
    public static final String INPUT_SEARCH_TEXT_PAYMENT_DATE = "search_text_payment_date";
    public static final String INPUT_SEARCH_TEXT_LCS_PRODUCT_CODE = "search_text_lcs_product_code";
    public static final String INPUT_SEARCH_TEXT_PGW_PRODUCT_CODE = "search_text_pgw_product_code";
    public static final String INPUT_SEARCH_TEXT_PRODUCT_NAME_ENGLISH = "search_text_product_name_english";
    public static final String INPUT_HIDDEN_DCM_ID = "hidden_dcm_id";
    public static final String INPUT_HIDDEN_DCM_NAME = "hidden_dcm_name";
    public static final String INPUT_HIDDEN_DCM_CODE = "hidden_dcm_code";
    public static final String INPUT_HIDDEN_REQUEST_CREATION_DATE = "hidden_request_creation_date";
    public static final String INPUT_HIDDEN_REQUEST_STATUS = "hidden_request_status";
    public static final String INPUT_HIDDEN_AMOUNT_PRICE = "hidden_price_amount";
    public static final String INPUT_HIDDEN_AMOUNT_PRICE_Multiply = "hidden_price_amount_multiply";
    public static final String INPUT_TEXT_REQUEST_PRODUCT_AMOUNT = "textrequestproductamount";
    public static final String INPUT_HIDDEN_DCM_QUOTA = "hidden_dcm_quota";
    public static final String INPUT_HIDDEN_MINIMUM_MAXIMUM_LIMIT = "hidden_minimum_maximum_limit";
    public static final String INPUT_TEXT_MINIMUM_LIMIT = "hidden_minimum_limit";
    public static final String INPUT_TEXT_MAXIMUM_LIMIT = "hidden_maximum_limit";
    public static final String INPUT_HIDDEN_REQUEST_ID = "hidden_request_id";
    public static final String INPUT_SELECT_REQUEST_STATUS = "select_request_status";
    public static final String INPUT_HIDDEN_OLD_REQUEST_STATUS = "hidden_old_request_status";
    public static final String INPUT_SELECT_EQUATION_STATUS = "select_equation_status";
    public static final String INPUT_SELECT_EQUATION_ID = "select_equation_id";
    public static final String INPUT_CHECKBOX_EQUATION_STOCK_PRODUCT = "checkbox_equation_stock_product";
    public static final String INPUT_SEARCH_TEXT_IMPORT_DATE_FROM = "search_text_import_date_from";
    public static final String INPUT_SEARCH_TEXT_IMPORT_DATE_TO = "search_text_import_date_to";
    public static final String INPUT_TEXT_ACTIVE_AMOUNT = "text_active_amount";
    public static final String INPUT_TEXT_PHYSICAL_AMOUNT = "text_physical_amount";
    public static final String INPUT_TEXT_PRODUCT_CODE = "text_product_code";
    public static final String INPUT_TEXT_PRODUCT_NAME_ENGLISH = "text_product_name_english";
    public static final String INPUT_TEXT_EQUATION_NAME = "text_equation_name";
    public static final String INPUT_TEXTAREA_EQUATION_DESCRIPTION = "textarea_equation_description";
    public static final String INPUT_HIDDEN_EQUATION_ID = "hidden_equation_id";
    public static final String INPUT_SELECT_PRODUCT_CODE = "select_product_code";
    public static final String INPUT_TEXT_DCM_QUOTA_VALUE = "text_dcm_quota_value";
    public static final String INPUT_TEXT_DCM_QUOTA_VALID_DAYS = "text_dcm_quota_valid_days";
    public static final String INPUT_TEXT_DCM_QUOTA_RECALCULATE_DAYS = "text_dcm_quota_recalculate_days";
    public static final String INPUT_HIDDEN_DCM_QUOTA_VALID_FROM = "hidden_dcm_quota_valid_from";
    public static final String INPUT_HIDDEN_DCM_QUOTA_VALID_TO = "hidden_dcm_quota_valid_to";
    public static final String INPUT_HIDDEN_DCM_QUOTA_VALUE_OLD = "hidden_dcm_quota_value_old";
    public static final String INPUT_TEXTAREA_DCM_QUOTA_CHANGE_REASON = "textarea_dcm_quota_change_reason";
    public static final String INPUT_HIDDEN_DCM_QUOTA_RECALCULATE_DAY = "hidden_dcm_quota_recalculate_day";
    public static final String INPUT_HIDDEN_REQUEST_NOTIFICATION_ID = "hidden_request_notification_id";
    public static final String INPUT_TEXT_PERSON_FULL_NAME = "text_person_full_name";
    public static final String INPUT_TEXT_PERSON_EMAIL = "text_person_email";
    public static final String INPUT_HIDDEN_CHANNEL_ID = "hidden_channel_id";
    public static final String INPUT_HIDDEN_CHANNEL_NAME = "hidden_channel_name";
    public static final String INPUT_HIDDEN_REPORT_ID = "hidden_report_id";
    public static final String INPUT_HIDDEN_WAREHOUSE_ID = "hidden_warehouse_id";
    public static final String INPUT_SEARCH_SELECT_WAREHOUSE_ID = "search_select_warehouse_id";
    public static final String CONTROL_HIDDEN_NAME_EQ_ELEMENT_TYPE_ID_PREFIX = "control_hidden_equation_element_type";
    public static final String CONTROL_HIDDEN_NAME_EQ_ELEMENT_VALUE_PREFIX = "control_hidden_equation_element_value";
    public static final String INPUT_TEXT_FRANCHISE_CODE = "franchise_code";
    public static final String INPUT_TEXT_ITEM_CODE = "item_code";
    public static final String INPUT_TEXT_FRANCHISE_DATE = "franchise_date";
    public static final String INPUT_TEXT_START_DATE = "start_date";
    public static final String INPUT_TEXT_END_DATE = "end_date";
    public static final String INPUT_TEXT_PGW_START_DATE = "pgw_start_date";
    public static final String INPUT_TEXT_PGW_END_DATE = "pgw_end_date";
    public static final String INPUT_TEXT_LCS_START_DATE = "lcs_start_date";
    public static final String INPUT_TEXT_LCS_END_DATE = "lcs_end_date";
    public static final String INPUT_HIDDEN_FRANCHISE_SEARCH_MODEL = "franchise_search_model";
    public static final String INPUT_HIDDEN_LCS_SEARCH_MODEL = "lcs_search_model";
    public static final String INPUT_HIDDEN_PGW_SEARCH_MODEL = "pgw_search_model";
    ////////////////////////////////////////////////////////////////////////
  /*
     * Tables and Views names /
     *//////////////////////////////////////////////////////////////////////
    public static final String TBL_SOP_PGW_VIEW = "SOP_PGW_VIEW";
    public static final String TBL_SOP_LCS_PHYSICAL_AMOUNT = "SOP_LCS_PHYSICAL_AMOUNT";
    ////////////////////////////////////////////////////////////////////////
  /*
     * Database Constants /
     *//////////////////////////////////////////////////////////////////////
    public static final String CONST_SCHEMA_STATUS_ACTIVE = "1";
    public static final String CONST_SCHEMA_STATUS_INACTIVE = "2";
    public static final String CONST_SCHEMA_STATUS_PREPARING = "3";
    public static final String CONST_REQUEST_STATUS_ACTIVE = "1";
    public static final String CONST_REQUEST_STATUS_REJECTED = "2";
    public static final String CONST_REQUEST_STATUS_DELETED = "3";
    public static final String CONST_REQUEST_STATUS_PAID = "4";
    public static final String CONST_REQUEST_STATUS_EXPIRED = "5";
    public static final String CONST_PRODUCT_LIMIT_IS_MANUAL = "1";
    public static final String CONST_EQUATION_OBJECT_TYPE_TERM = "1";
    public static final String CONST_EQUATION_OBJECT_TYPE_OPERATOR = "2";
    public static final String CONST_EQUATION_OBJECT_TYPE_FACTOR = "3";
    public static final String CONST_GEN_CHANNEL_DISTRIBUTION = "1";
    public static final String CONST_GEN_CHANNEL_FRANCHISE = "2";
    public static final String CONST_GEN_COMPLEMENTARY_CHANNEL = "3";
    public static final String CONST_GEN_AUTHORIZED_AGENT = "4";
    public static final String CONST_SCRATCH_REPROT = "1";
    public static final String CONST_PRODUCT_REPORT = "2";
    public static final String CONST_TOTAL_REPRT = "3";
    public static final String PRODUCT_VECTOR = "product_vector";
    public static final String PRODUCT_REPORT = "product_report";
    public static final String PRODUCT_CHANNEL = "product_channel";
    public static final String PRODUCT_ASSIGNED_VECTOR = "product_assigned_vector";
    public static final String CONTROL_HIDDEN_PRODUCT_CHECKED = "control_hidden_product_checked";
    public static final String CONTROL_HIDDEN_REPORT_ID = "control_hidden_report_id";
    public static final String CONTROL_HIDDEN_CHANNEL_ID = "control_hidden_channel_id";
    ////////////////////////////////////////////////////////////////////////
  /*
     * CACH OBJECTS /
     *//////////////////////////////////////////////////////////////////////
    public static final String CACH_OBJ_DCM_LIST_SOP = "cach_obj_dcm_list_sop";
    public static final String PAGE_HEADER = "page_header";
    public static final String ACTIVE_SCHEMA_EXISTS = "active_schema_exists";
    public static final String FRANCHISE_ITEM_PGW = "franchise_item_pgw";
    public static final String FRANCHISE_ITEM_LCS = "franchise_item_lcs";
}