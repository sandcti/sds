package com.mobinil.sds.web.interfaces.cam;

public class DeductionInterfaceKey 
{
  public static final String ACTION_VIEW_IMPORT_DEDUCTION="view_import_deduction";
  public static final String ACTION_IMPORT_DEDUCTION="import_deduction";
  public static final String ACTION_VIEW_DEDUCTION_SEARCH="view_deduction_search";
  public static final String ACTION_VIEW_DEDUCTION="view_deduction";
  public static final String ACTION_VIEW_DEDUCTION_SEARCH_FOR_VIEW="view_deduction_search_for_view";
  public static final String ACTION_VIEW_DEDUCTION_FOR_VIEW="view_deduction_for_view";
  public static final String ACTION_EDIT_DEDUCTION="edit_deduction";
  public static final String ACTION_UPDATE_DEDUCTION="update_deduction";
  public static final String ACTION_DELETE_DEDUCTION="delete_deduction";
  public static final String ACTION_UDPATE_DEDUCTION_STATUSES="update_deduction_statuses";
   public static final String ACTION_VIEW_REASON_SEARCH="view_reason_search";
  public static final String ACTION_VIEW_REASON="view_reason";
  public static final String ACTION_NEW_REASON="new_reason";
  public static final String ACTION_ADD_REASON="add_reason";
  public static final String ACTION_EDIT_REASON="edit_reason";
  public static final String ACTION_UPDATE_REASON="update_reason";
  public static final String ACTION_UPDATE_REASON_STATUS="update_reason_status";
  public static final String ACTION_PAYMENT_SUMMERY_REPORT="payment_summery_report";

  public static final String CONTROL_DEDUCTION_ID = "deduction_id";
  public static final String CONTROL_SELECT_REASON = "reason_select";
  public static final String CONTROL_SELECT_STATUS = "status_select";
  public static final String CONTROL_SELECT_PAY_GROUP_TYPE = "group_type_select";
  public static final String CONTROL_TEXT_DEDUCTION_VALUE  = "deduction_value";
  public static final String CONTROL_TEXT_DEDUCTION_REMAIN_VALUE = "deduction_remaining_value";
  public static final String CONTROL_SELECT_SEARCH_REASON = "reason_search_select";
  public static final String CONTROL_SELECT_SEARCH_STATUS = "status_search_select";
  public static final String CONTROL_SELECT_SEARCH_PAY_GROUP_TYPE = "group_type_search_select";
  public static final String CONTROL_HIDDEN_REASON_ID = "reason_id";
  public static final String CONTROL_TEXT_REASON_NAME = "reason_name";
  public static final String CONTROL_TEXT_REASON_DESC = "reason_desc";
  public static final String CONTROL_SELECT_REASON_STATUS = "reason_status";
  public static final String CONTROL_TEXT_SEARCH_REASON_NAME = "reason_name_search";
public static final String CONTROL_SELECT_SEARCH_REASON_STATUS = "reason_status_search";
public static final String CONTROL_SEARCH_DATE_FROM = "search_date_from";
public static final String CONTROL_SEARCH_DATE_TO = "search_date_to";
  
  public static final String VECTOR_DEDUCTION = "deduction_vec";
  public static final String VECTOR_DEDUCTION_REASON = "ded_reason_vec";
  public static final String VECTOR_DEDUCTION_STATUS= "ded_status_vec";
  public static final String VECTOR_PAY_GROUP_TYPE= "ded_group_type_vec";
  public static final String VECTOR_REASON_STATUS= "reason_status_vec";
  public static final String HASHMAP_DEDUCTION_MAKER_ACTIONS_DETAILS= "deduction_maker_actions_details";

  public static final String MODEL_DEDUCTION= "deduction_model";
  public static final String MODEL_DEDUCTION_REASON= "deduction_model_reason";
}