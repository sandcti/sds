package com.mobinil.sds.web.interfaces.cam;

public class AccrualInterfaceKey 
{
   public static final String ACTION_VIEW_ALL_ACCRUAL="view_all_accrual";
   public static final String ACTION_NEW_ACCRUAL="new_accrual";
   public static final String ACTION_EDIT_ACCRUAL="edit_accrual";
   public static final String ACTION_ADD_ACCRUAL="add_accrual";
   public static final String ACTION_UPDATE_ACCRUAL="update_accrual";
   public static final String ACTION_UPDATE_ACCRUAL_STATUSES="update_accrual_statuses";
   public static final String ACTION_VIEW_MAKER_DATE_ENTRY = "add_maker_value";
   public static final String ACTION_DELETE_ACCRUAL_VALUE = "delete_maker_value";
   public static final String SEARCH_CHECKER_MANAGEMENT = "search_checker_management";
   public static final String VIEW_CHECKER_MANAGEMENT = "view_checker_management";
   public static final String ACTION_SEARCH_ACCRUAL = "search_accrual";
   public static final String ACTION_CHECKER_APPROVE_ACCRUAL_VALUE = "checker_approve_value";
   

   public static final String ACTION_VIEW_REASON_SEARCH="view_accrual_reason_search";
   public static final String ACTION_VIEW_REASON="view_accrual_reason";
   public static final String ACTION_NEW_REASON="new_accrual_reason";
   public static final String ACTION_ADD_REASON="add_accrual_reason";
   public static final String ACTION_EDIT_REASON="edit_accrual_reason";
   public static final String ACTION_UPDATE_REASON="update_accrual_reason";
   public static final String ACTION_UPDATE_REASON_STATUS="update_accrual_reason_status";
   
   public static final String VECTOR_ALL_ACCRUAL = "all_accrual_vec";
   public static final String VECTOR_ALL_ACCRUAL_STATUS = "all_accrual_status_vec";
   public static final String VECTOR_ALL_DCM_CHANNEL = "all_dcm_channel";
   public static final String VECTOR_ALL_REASONS = "all_reasons";
   public static final String VECTOR_ALL_MAKER_VALUE = "all_maker_value";
   public static final String VECTOR_REASON_STATUS= "reason_status_vec";

   
   public static final String CONTROL_HIDDEN_ACCRUAL_ID="accrual_id";
   public static final String CONTROL_TEXT_ACCRUAL_NAME="accrual_name_text";
   public static final String CONTROL_TEXT_ACCRUAL_DESCRIPTION="accrual_desc_text";
   public static final String CONTROL_TEXT_ACCRUAL_VALUE="accrual_value_text";
   public static final String CONTROL_SELECT_ACCRUAL_CHANNEL="accrual_channel_list";
   public static final String CONTROL_SELECT_ALL_ACCRUAL_STATUS="accrual_status_list";
   public static final String CONTROL_SELECT_SEARCH_ALL_ACCRUAL_STATUS="accrual_search_status_list";
   public static final String CONTROL_SEARCH_VALUE_TYPE="search_value_type";
   public static final String CONTROL_TEXT_SEARCH_NAME="search_name_key";
   public static final String CONTROL_SELECT_CHANNEL_SEARCH="channel_search_key";
   public static final String CONTROL_SELECT_STATUS_SEARCH="status_search_key";
   public static final String CONTROL_HIDDEN_REASON_ID = "reason_id";
   public static final String CONTROL_TEXT_REASON_NAME = "reason_name";
   public static final String CONTROL_TEXT_REASON_DESC = "reason_desc";
   public static final String CONTROL_SELECT_REASON_STATUS = "reason_status";
   public static final String CONTROL_TEXT_SEARCH_REASON_NAME = "reason_name_search";
   public static final String CONTROL_SELECT_SEARCH_REASON_STATUS = "reason_status_search";

   public static final String MODEL_ACCRUAL = "accrual_model";
   public static final String MODEL_ACCRUAL_REASON= "accrual_reason_model";
}