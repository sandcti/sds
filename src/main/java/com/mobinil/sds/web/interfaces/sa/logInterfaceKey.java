package com.mobinil.sds.web.interfaces.sa;

public interface logInterfaceKey 
{
// Key for the select control name prefix
  public static final String CONTROL_INPUT_NAME_FROM_DATE = "from";  

// Key for the select control name prefix
  public static final String CONTROL_INPUT_NAME_TO_DATE = "to";  

// Key for the select control name prefix
  public static final String CONTROL_INPUT_NAME_USER_ID = "user_log_id";  

// Key for the select control name prefix
  public static final String CONTROL_INPUT_NAME_USER_IP = "user_ip";  

  // Key for the select control name prefix
  public static final String CONTROL_INPUT_NAME_ACTION_NAME = "action_name";  

  // Key for the select control name prefix
  public static final String CONTROL_INPUT_NAME_ACTION_URL = "action_url";  
}