package com.mobinil.sds.web.interfaces.IFReportDelivery;

public interface IFReportDeliveryInterfaceKey {
	public static final String ACTION_VIEW_EXPORT_MOBINIL_REPORT_TO_INFO="view_export_mobinil_report_to_info_fort";
	public static final String ACTION_SEARCH_EXPORT_MOBINIL_REPORT_TO_INFO="search_export_mobinil_report_to_info_fort";
	public static final String ACTION_EXPORT_MOBINIL_REPORT_TO_INFO="export_mobinil_report_to_info_fort";
	
	public static final String ACTION_EXPORT_JOB_DETAILS_PAGE="export_job_details_page";
	public static final String ACTION_GENERATE_CHANGE_ERROR_TYPE_TEMPLATE="generate_change_error_type_template";
	
	public static final String ACTION_VIEW_IMPORT_INFO_FORT_REPORT="view_import_info_fort_report";
	public static final String ACTION_IMPORT_INFO_FORT_REPORT="import_info_fort_report";
	
	
	public static final String ACTION_VIEW_IMPORT_CHANGE_RECORD_TYPE="view_import_change_record_type";
	public static final String ACTION_IMPORT_CHANGE_RECORD_TYPE_TEMPLATE="import_change_record_type_template";
	
	public static final String ACTION_VIEW_DATA_REPORTS_JOBS="view_data_reports_jobs";
	public static final String ACTION_VIEW_JOB_DETAILS="view_job_details";
	
	public static final String ACTION_CLOSE_JOB_DETAILS="close_job_details";
	public static final String ACTION_EXPORT_NOT_FOUND="export_not_found";
	public static final String ACTION_EXPORT_DIST_ERRORS="export_dist_errors";
	public static final String ACTION_EXPORT_INFO_FORT_ERRORS="export_info_fort_errors";
	public static final String ACTION_EXPORT_DUPLICATES="export_duplicates";
	public static final String ACTION_EXPORT_AUTO_MATCHED="export_auto_matched";
	public static final String ACTION_DELETE_JOB_DETAILS="DELETE_job_details";
	public static final String ACTION_SEARCH_JOBS="search_jobs";
	
	public static final String ACTION_UPDATE_JOB_DETAILS="update_job_details";
	
	
	public static final String CONTROL_BATCH_ID="batch_id";
	public static final String CONTROL_HIDDEN_BATCH_ID="hidden_batch_id";
	public static final String CONTROL_HIDDEN_JOB_ID="hidden_job_id";
	public static final String CONTROL_JOB_ID="job_id";
	public static final String CONTROL_BATCH_DATE="batch_date";
	public static final String CONTROL_BATCH_TYPE="batch_type";
	public static final String CONTROL_BATCH_STATUS="batch_status";
	public static final String CONTROL_DCM_NAME="dcm_name";
	public static final String CONTROL_SELECT_RECORD_TYPE="select_record_type";
	
	public static final String CONTROL_SELECT_JOB_STATUS="select_job_status";
	
	public static final String CONTROL_REPORT_CREATION_DATE="control_report_creation_date";
	
	public static final String CONTROL_JOB_CREATION_DATE="control_job_creation_date";
	
	public static final String VECTOR_SEARCH_BATCHES="search_batches";
	
	public static final String VECTOR_BATCH_TYPES="batch_types";
	public static final String VECTOR_JOBS="vector_jobs";
	
	public static final String VECTOR_JOB_STATUSES="vector_job_statuses";
	
	public static final String VECTOR_INFO_FORT_REPORTS="vector_info_fort_reports";
	public static final String VECTOR_DATA_RECORD_TYPES="vector_data_record_types";
	
	
	public static final String First_Time_Search="first_time_search";
	public static final String CONTROL_HIDDEN_PAGE_NUMBER="hidden_page_number";
	public static final String CONTROL_HIDDEN_TOTAL_PAGES="hidden_total_pages";
	public static final String First_Time_Search_FALSE="first_time_search_false";
}
