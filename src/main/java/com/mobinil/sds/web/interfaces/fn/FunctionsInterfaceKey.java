package com.mobinil.sds.web.interfaces.fn;

public interface FunctionsInterfaceKey  {

public static final String CACH_OBJ_FUNCTION_STATUS = "cach_obj_function_status";
public static final String CACH_OBJ_FUNCTION_TYPES = "cach_obj_function_types";
public static final String CACH_OBJ_FUNCTION_LIST = "cach_obj_function_list";

///////////////List of action keys//////////////////////
public static final String FUNCTION_ACTION = "function_action";
public static final String HASHMAP_KEY_FUNCTION_COLLECTION = "hashmap_key_function_collection";
public static final String HASHMAP_KEY_FUNCTION_PARAMETER_COLLECTION = "hashmap_key_function_parameter_collection";

///////////////List of actions for functions///////////////
public static final String ACTION_SHOW_ALL_FUNCTIONS   = "show_all_functions";
public static final String ACTION_ADD_FUNCTION   = "add_function";
public static final String ACTION_EDIT_FUNCTION   = "edit_function";
public static final String ACTION_SAVE_FUNCTION   = "save_function";

///////////////List of controls///////////////////////// 
public static final String CONTROL_HIDDEN_FUNCTION_ID = "hidden_function_id" ;
public static final String CONTROL_TEXTAREA_FUNCTION_NAME = "textarea_function_name";
public static final String CONTROL_SELECT_FUNCTION_STATUS = "select_function_status";
public static final String CONTROL_SELECT_FUNCTION_TYPE = "select_function_type";
public static final String CONTROL_TEXT_FUNCTION_SQL_CALL = "text_function_sql_call";
public static final String CONTROL_TEXTAREA_FUNCTION_HELP_TEXT = "textarea_function_help_text";
public static final String CONTROL_TEXTAREA_FUNCTION_DESCRIPTION = "textarea_function_description";
}