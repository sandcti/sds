package com.mobinil.sds.core.system.request.model;

public class SupervisorModel 
{
	private int supervisorId;
	private String supervisorName;
	private int supervisorStatus;
	
	
	
	public int getSupervisorId() {
		return supervisorId;
	}
	public void setSupervisorId(int supervisorId) {
		this.supervisorId = supervisorId;
	}
	public String getSupervisorName() {
		return supervisorName;
	}
	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}
	public int getSupervisorStatus() {
		return supervisorStatus;
	}
	public void setSupervisorStatus(int supervisorStatus) {
		this.supervisorStatus = supervisorStatus;
	}
	
	

}
