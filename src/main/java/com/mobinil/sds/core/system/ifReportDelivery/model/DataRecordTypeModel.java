package com.mobinil.sds.core.system.ifReportDelivery.model;

public class DataRecordTypeModel {
	int typeId;
	String typeName;
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	

}
