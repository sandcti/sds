package com.mobinil.sds.core.system.ifReportDelivery.model;

public class InfoFortDataModel {
private int jobId;
private int batchId;
private String dcmName;
private int sheetSerial;
private String posCode;
private String simSerial;
private int dial;
private String infoFort1stName;
private String infoFort2ndName;
private String infoFort3rdName;
private String infoFort4thName;
private int contractId;
private String contract1stName;
private String contract2ndName;
private String contractlstName;
private int recordTypeId;
private String recordType;

public InfoFortDataModel() {
	super();
	// TODO Auto-generated constructor stub
}
public int getJobId() {
	return jobId;
}
public void setJobId(int jobId) {
	this.jobId = jobId;
}
public int getBatchId() {
	return batchId;
}
public void setBatchId(int batchId) {
	this.batchId = batchId;
}
public String getDcmName() {
	return dcmName;
}
public void setDcmName(String dcmName) {
	this.dcmName = dcmName;
}
public int getSheetSerial() {
	return sheetSerial;
}
public void setSheetSerial(int sheetSerial) {
	this.sheetSerial = sheetSerial;
}
public String getPosCode() {
	return posCode;
}
public void setPosCode(String posCode) {
	this.posCode = posCode;
}
public String getSimSerial() {
	return simSerial;
}
public void setSimSerial(String simSerial) {
	this.simSerial = simSerial;
}
public int getDial() {
	return dial;
}
public void setDial(int dial) {
	this.dial = dial;
}
public String getInfoFort1stName() {
	return infoFort1stName;
}
public void setInfoFort1stName(String infoFort1stName) {
	this.infoFort1stName = infoFort1stName;
}
public String getInfoFort2ndName() {
	return infoFort2ndName;
}
public void setInfoFort2ndName(String infoFort2ndName) {
	this.infoFort2ndName = infoFort2ndName;
}
public String getInfoFort3rdName() {
	return infoFort3rdName;
}
public void setInfoFort3rdName(String infoFort3rdName) {
	this.infoFort3rdName = infoFort3rdName;
}
public String getInfoFort4thName() {
	return infoFort4thName;
}
public void setInfoFort4thName(String infoFort4thName) {
	this.infoFort4thName = infoFort4thName;
}
public int getContractId() {
	return contractId;
}
public void setContractId(int contractId) {
	this.contractId = contractId;
}
public String getContract1stName() {
	return contract1stName;
}
public void setContract1stName(String contract1stName) {
	this.contract1stName = contract1stName;
}
public String getContract2ndName() {
	return contract2ndName;
}
public void setContract2ndName(String contract2ndName) {
	this.contract2ndName = contract2ndName;
}
public String getContractlstName() {
	return contractlstName;
}
public void setContractlstName(String contractlstName) {
	this.contractlstName = contractlstName;
}
public int getRecordTypeId() {
	return recordTypeId;
}
public void setRecordTypeId(int recordTypeId) {
	this.recordTypeId = recordTypeId;
}
public String getRecordType() {
	return recordType;
}
public void setRecordType(String recordType) {
	this.recordType = recordType;
}


}
