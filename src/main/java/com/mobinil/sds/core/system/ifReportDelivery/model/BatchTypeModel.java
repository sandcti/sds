package com.mobinil.sds.core.system.ifReportDelivery.model;

public class BatchTypeModel {
	private int batchTypeId;
	private String batchTypeName;
	public int getBatchTypeId() {
		return batchTypeId;
	}
	public void setBatchTypeId(int batchTypeId) {
		this.batchTypeId = batchTypeId;
	}
	public String getBatchTypeName() {
		return batchTypeName;
	}
	public void setBatchTypeName(String batchTypeName) {
		this.batchTypeName = batchTypeName;
	}
	

}
