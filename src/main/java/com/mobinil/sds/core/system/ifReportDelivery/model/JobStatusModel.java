package com.mobinil.sds.core.system.ifReportDelivery.model;

public class JobStatusModel {
	private int jobStatusId;
	private String jobStatusName;
	public int getJobStatusId() {
		return jobStatusId;
	}
	public void setJobStatusId(int jobStatusId) {
		this.jobStatusId = jobStatusId;
	}
	public String getJobStatusName() {
		return jobStatusName;
	}
	public void setJobStatusName(String jobStatusName) {
		this.jobStatusName = jobStatusName;
	}
	

}
