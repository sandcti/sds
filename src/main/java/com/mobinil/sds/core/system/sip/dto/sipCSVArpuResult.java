package com.mobinil.sds.core.system.sip.dto;

public class sipCSVArpuResult
{
   private Integer arpu;
   private Integer count;
   public Integer getArpu()
   {
      return arpu;
   }
   public void setArpu(Integer arpu)
   {
      this.arpu = arpu;
   }
   public Integer getCount()
   {
      return count;
   }
   public void setCount(Integer count)
   {
      this.count = count;
   }
}
