/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mobinil.sds.core.system.scm.model;

/**
 *
 * @author Ahmed Adel
 */
public class POSSimilar {
    private String POSCode;
    private String POSName;
    private String POSAddress;

    /**
     * @return the POSCode
     */
    public String getPOSCode() {
        return POSCode;
    }

    /**
     * @param POSCode the POSCode to set
     */
    public void setPOSCode(String POSCode) {
        this.POSCode = POSCode;
    }

    /**
     * @return the POSName
     */
    public String getPOSName() {
        return POSName;
    }

    /**
     * @param POSName the POSName to set
     */
    public void setPOSName(String POSName) {
        this.POSName = POSName;
    }

    /**
     * @return the POSAddress
     */
    public String getPOSAddress() {
        return POSAddress;
    }

    /**
     * @param POSAddress the POSAddress to set
     */
    public void setPOSAddress(String POSAddress) {
        this.POSAddress = POSAddress;
    }

}
