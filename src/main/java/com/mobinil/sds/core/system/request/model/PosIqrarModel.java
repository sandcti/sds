/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mobinil.sds.core.system.request.model;

/**
 *
 * @author Salma
 */
public class PosIqrarModel
{
    private String posName;
    private String posCode;
    private String ownerName;
    private String ownerIdNo;
    private String districName;
    private String areaName;
    private String stkNo;

    /**
     * @return the posName
     */
    public String getPosName() {
        return posName;
    }

    /**
     * @param posName the posName to set
     */
    public void setPosName(String posName) {
        this.posName = posName;
    }

    /**
     * @return the posCode
     */
    public String getPosCode() {
        return posCode;
    }

    /**
     * @param posCode the posCode to set
     */
    public void setPosCode(String posCode) {
        this.posCode = posCode;
    }

    /**
     * @return the ownerName
     */
    public String getOwnerName() {
        return ownerName;
    }

    /**
     * @param ownerName the ownerName to set
     */
    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    /**
     * @return the ownerIdNo
     */
    public String getOwnerIdNo() {
        return ownerIdNo;
    }

    /**
     * @param ownerIdNo the ownerIdNo to set
     */
    public void setOwnerIdNo(String ownerIdNo) {
        this.ownerIdNo = ownerIdNo;
    }

    /**
     * @return the districName
     */
    public String getDistricName() {
        return districName;
    }

    /**
     * @param districName the districName to set
     */
    public void setDistricName(String districName) {
        this.districName = districName;
    }

    /**
     * @return the areaName
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * @param areaName the areaName to set
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * @return the stkNo
     */
    public String getStkNo() {
        return stkNo;
    }

    /**
     * @param stkNo the stkNo to set
     */
    public void setStkNo(String stkNo) {
        this.stkNo = stkNo;
    }

}
