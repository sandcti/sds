/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mobinil.sds.core.system.sa.history.dao;

import com.mobinil.sds.core.utilities.Utility;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author sand
 */
public class PaymentLevelHistoryDao {
    
  private  PaymentLevelHistoryDao()
  {
  }

  public static boolean checkHistoryFile(String month , String year, String userId )
  {
        boolean exists =false;

        try
        {
          Connection con = Utility.getConnection();
          Statement stat = con.createStatement();
          String sql = "select * from GEN_DCM_PAYMENT_LEVEL_HISTORY where user_id = '"+userId+"' and year ='"+year+"' and month='"+month+"'";
          ResultSet res = stat.executeQuery(sql);
          if(res.next())
          {
           exists = true;
          }
          Utility.closeConnection(con);      
        }
        catch(Exception e)
        {
        e.printStackTrace();
        }
        return exists;
  }
  public static void invalidateHistoryFile(String month , String year, String userId )
  {
      try
        {
          Connection con = Utility.getConnection();
          Statement stat = con.createStatement();
          String sql = "update GEN_DCM_PAYMENT_LEVEL_HISTORY set status_id='2' where user_id = '"+userId+"' and year='"+year+"' and month ='"+month+"'";
          stat.executeQuery(sql);
          sql="commit";
          stat.executeUpdate(sql);
          Utility.closeConnection(con);      
        }
        catch(Exception e)
        {
        e.printStackTrace();
        }
  }
  public static String getHistoryFileId(String month , String year, String userId )
  {
      String historyFileId = "";

        try
        {
          Connection con = Utility.getConnection();
          Statement stat = con.createStatement();
          String sql = "select history_file_id from GEN_DCM_PAYMENT_LEVEL_HISTORY where user_id = '"+userId+"' and year ='"+year+"' and month='"+month+"'";
          ResultSet res = stat.executeQuery(sql);
          if(res.next())
          {
            historyFileId = res.getString("history_file_id");
          }
          Utility.closeConnection(con);      
        }
        catch(Exception e)
        {
        e.printStackTrace();
        }
        return historyFileId;
  }
  public static void insertHistoryFile(String month , String year, String userId )
  {
   
    try
        {
          Connection con = Utility.getConnection();
          Statement stat = con.createStatement();
          String sql = "insert into gen_dcm_payment_level_history (user_id,status_id, month,year) values ('"+userId+"','1','"+month+"','"+year+"')";
          stat.executeUpdate(sql);
          sql="commit";
          stat.executeUpdate(sql);
          Utility.closeConnection(con);      
        }
        catch(Exception e)
        {
        e.printStackTrace();
        }

    
  }
  public static void insertHistoryFileDetail(String historyId)
  {
   
    try
        {
          Connection con = Utility.getConnection();
          Statement stat = con.createStatement();
          String sql = "insert into dcm_payment_level_hist_detail select '"+historyId+"', dcm_id, dcm_code,dcm_payment_level_id, channel_id from gen_dcm";
          stat.executeUpdate(sql);
          sql="commit";
          stat.executeUpdate(sql);
          Utility.closeConnection(con);      
        }
        catch(Exception e)
        {
        e.printStackTrace();
        }

    
  }
}
    

